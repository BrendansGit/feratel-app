'use strict';
var camDetailIndex, jsonCams, jsonOrte, jsonMenu, jsonLoading = 0, extraInfoCache, userpos = [], menuCache = {}, mySwipe, camsNearByCenter, fromDetailMapLink, loadContentT, pistenplanUrl, iframeLoaded = 0, rating = [], globalSlideIndex = 0, loadedSlides = {};
var piwikbaseurl = 'http://webstat02.feratel.at/';
// var piwikbaseurl = 'http://appicc.com/piwik/piwik/';
var _paq;
var toTime = 30 * 1000;
var trackinterval;
var trackedID, trackedIDs = [];
var trackedCams = [];
var lastCamsTracked = 0;
var actTrack = 0;
var detalCdata = {};
rating.rated = localStorage.rating_done;
rating.open = 0;
rating.said_no = localStorage.denied;
rating.version = '1.5.1';
var dateNow = new Date().getTime();
var lastUpdate = parseInt(localStorage.lastUpdate);
// console.log(lastUpdate);
var savePage;
// window.location.href = ('#/main.html');
function ua(source) {
  var match = navigator.userAgent.toLowerCase().indexOf(source);
  if (match != -1) {
    match = true;
  } else {
    match = false;
  }
  return match;
}
var android = ua('android');
var iphone = ua('iphone');
var ipad = ua('ipad');
var horizontal = 0, vertical = 0;
var setWidthSlide = 0;
if (localStorage['rating_time'] === undefined || localStorage['rating_time'] == 'NaN') {
  localStorage['rating_time'] = 1;
} else {
  localStorage['rating_time'] = parseInt(localStorage['rating_time']) + 1;
}
// console.log(localStorage['rating_time']);
if (parseInt(localStorage['rating_time']) >= 8 && parseInt(localStorage['rating_time']) < 15) {
  rating.open = 1;
}
if (rating.open) {
  setTimeout(function () {
    customAlert(1, 'rate');
  }, 10000);
}
var actMenu;
var actMenuLevel;
// localStorage.clear();
// set "global" function for error evasion
var setCamDetail = function () {
};
var goToC = function () {
};
var goBack = function () {
};
var shareApp = function () {
  if (typeof window.plugins !== 'undefined') {
    window.plugins.socialsharing.share('feratel Webcam App ', null, null, 'http://www.feratel.com/officialapp');
  } else {
    console.log('http://www.feratel.com/officialapp');
  }
};
var log = function (t) {
  $('.debug').text(t);
};
var setPageHeight = function (intH, clss) {
  if (intH == 'detailPage') {
    intH = $(clss).height() + 200 + $('.titleBox').height();
  }
  if (intH == 0) {
    intH = 'auto';
  }
  $('.Page').height(intH);  // log(intH);
};
var pageScroll = function (is) {
  if (is === 1) {
    $('.mb-page').removeClass('noScroll');
  } else {
    scrollto(0, 1);
    $('.mb-page').addClass('noScroll');
  }
};
function bmForm(obj) {
  var nodouble = obj[0].hasBmForm;
  var d = new Date();
  // add a day
  d.setDate(d.getDate() + 1);
  var curr_date = d.getDate();
  var curr_month = d.getMonth() + 1;
  var curr_year = d.getFullYear();
  if (curr_month.toString().length == 1) {
    curr_month = '0' + curr_month;
  }
  if (curr_date.toString().length == 1) {
    curr_date = '0' + curr_date;
  }
  var d_date = curr_year + '-' + curr_month + '-' + curr_date;
  setTimeout(function () {
    $('#anreise').val(d_date);
  }, 1);
  if (nodouble !== 1) {
    var $rows = obj.find('.row');
    $rows.each(function (rowc) {
      var $children = $(this).children();
      $children.each(function (childc) {
        var label = $(this).attr('label'), stickLabel = $(this).attr('label-stick'), cname = 'split-' + $children.length, indexName = 'part-' + (childc + 1), thisInput = $children[childc], inputType = thisInput.type, localName = thisInput.nodeName.toLowerCase();
        if (label != undefined && label.indexOf(',') !== -1) {
          label = label.split(',')[0];
        }
        $(this).attr('placeholder', label);
        $(this).wrap('<div class="split ' + cname + ' ' + indexName + '"><div class="style-box ' + localName + '-style">');
        if (stickLabel == 'true') {
        }
        if (label !== undefined) {
          // if (localName == 'input') {
          var $this = $(this);
          var label = $(this).attr('label');
          if (inputType == 'date') {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            if (mm.toString().length == 1) {
              mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            var nowDate = yyyy + '-' + mm + '-' + dd;
            $(this).val(nowDate);
            $this[0].defaultValue = nowDate;
          }
          if (localName == 'button') {
            $(this).parent().addClass('buttonBox');
          }
          $this.parent().before('<span class="bmlabel">' + label + '</span>');
          $this.on('focus click', function () {
            $(this).val('');
            $this.addClass('bgf');
          }).on('blur change', function () {
            if ($this.val()) {
              if ($this.val().length < 1) {
                $this.val($this[0].defaultValue);
              } else {
                $this[0].defaultValue = $this.val();
              }
            }
          });
        }  // $(this).addClass('bmForm-node-'+(childc+1));
      });
    });
    obj.find('input').each(function () {
      var stickLabel = $(this).attr('label-stick');
      if (stickLabel == 'true') {
        $(this).on('focus', function () {
        }).on('blur', function () {
        });
      }
    });
    obj[0].hasBmForm = 1;
  }
}
;
var openLink = function (link) {
  // window.open(link, '_blank');
  window.open(link, '_blank', 'clearcache=yes,location=yes,clearsessioncache=yes');  // window.location.href= link;
};
var openLinkO = function (link) {
  window.open(link, '_system');
};
var customAlert = function (data, what) {
  if (data === 0) {
    $('.custom-alert-container').fadeOut(200);
  }
  if (data === 1) {
    if (what == 'rate') {
      $('.rateingBox').css('display', 'block').siblings().css('display', 'none');
    } else if (what == 'con') {
      $('.connectionTimeout').css('display', 'block').siblings().css('display', 'none');
    } else if (what == 'loc') {
      $('.enableLocation').css('display', 'block').siblings().css('display', 'none');
    } else if (what == 'custom') {
      $('.dynamicNotification').css('display', 'block').siblings().css('display', 'none');
    }
    $('.custom-alert-container').fadeIn(400);
  }
};
var maTO;
var miniAlert = function (content) {
  // console.log(content);
  var $ma = $('.miniAlert');
  $ma.stop(!0, !0).fadeOut(0, function () {
    $ma.find('span').text('');
  });
  $ma.find('span').html(content);
  $ma.css('margin-left', '-' + $ma.width() / 2 + 'px');
  $ma.stop(!0, !0).fadeIn(200);
  clearTimeout(maTO);
  maTO = setTimeout(function () {
    $ma.fadeOut(200, function () {
      $ma.find('span').text('');
    });
  }, 2200);
};
window.checkOrientation = function () {
  if (window.outerHeight + 200 > window.outerWidth) {
    $('body').addClass('vertical').removeClass('horizontal');
    vertical = 1;
    horizontal = 0;
  } else {
    $('body').addClass('horizontal').removeClass('vertical');
    vertical = 0;
    horizontal = 1;
  }
};
var udevice;
if (iphone) {
  udevice = 'iPhone';
}
if (android) {
  udevice = 'Android';
}
if (ipad) {
  udevice = 'iPad';
}
window.trackData = {
  device: udevice,
  camdata: {},
  camid: ''
};
var totalTrackHistory = {};
var lstTrack = new Date().getTime();
function pauseall() {
  clearTimeout(trackinterval);
}
document.addEventListener('pause', pauseall, false);
function trackCam() {
  trackedID = '-1', trackedIDs = [];
  // var camdetail = jQuery.parseJSON(localStorage.camDetailIndex);
  // var cdata = {
  //     'kameraid': camdetail.id,
  //     'sprache': t.pageLang,
  //     'portalname': 'APP_feratel.com',
  //     'area': 'cam',
  //     'other': device
  // };
  var adi = jQuery.parseJSON(localStorage.camDetailIndex);
  if (lastCamsTracked !== adi.id) {
    trackedCams.length = 0;
    trackedCams = [];
  }
  lastCamsTracked = adi.id;
  clearInterval(trackinterval);
  trackinterval = setInterval(function () {
    $('.sls').children().each(function () {
      var os = $(this).offset().left;
      if (os == 0) {
        var id = $(this).attr('data-index');
        if (localStorage.camDetailIndex !== undefined) {
          var dinfo = adi.detail;
          var actid = parseInt(dinfo['110'][id][0]);
          var hasTracked = 1;
          for (var i = 0; i < trackedCams.length; i++) {
            if (trackedCams[i] == actid) {
              hasTracked = 0;
            }
          }
          var dateNow = new Date().getTime();
          var latsTracked = Infinity;
          if (totalTrackHistory[actid] !== undefined) {
            latsTracked = dateNow - totalTrackHistory[actid];  // console.log(totalTrackHistory[actid]);
          }
          if (parseInt(trackedID) !== actid && hasTracked && latsTracked > toTime) {
            detalCdata.kameraid = dinfo['110'][id][0];
            trackedID = parseInt(dinfo['110'][id][0]);
            actTrack = id;
            trackedCams.push(trackedID);
            totalTrackHistory[actid] = new Date().getTime();
            var icctrack = 'http://appicc.com/feratel/app/icctrack.php?id=' + trackedID;
            icctrack += '&n=' + encodeURIComponent(adi.name);
            icctrack += '&ua=' + encodeURIComponent(navigator.userAgent);
            icctrack += '&l=' + localStorage.lang;
            if (typeof window.device !== 'undefined') {
              var deviceID = window.device.uuid;
              icctrack += '&uid=' + deviceID;
            }
            $.get(icctrack, function (e) {
            });
            window.trackPiwik(detalCdata, 'cam');
          }
        }
      }
    });
  }, 500);
}
;
window.trackPiwik = function (cdata, cam) {
  // console.log(cdata, cam);
  if (cdata.kameraid.length == 0) {
    trackedCams.length = 0;
    trackedCams = [];
  }
  var isdetail = window.location.hash.indexOf('detail.html');
  // if (typeof id !== 'undefined') {
  //   cdata.kameraid = detailInfo['110'][id][0];
  // };
  detalCdata = cdata;
  if (typeof cam != 'undefined') {
    if (localStorage.camDetailIndex !== undefined && isdetail > -1) {
      var detailInfo = jQuery.parseJSON(localStorage.camDetailIndex).detail;
      if (detailInfo['110'][actTrack] !== undefined) {
        cdata.kameraid = detailInfo['110'][actTrack][0];
      }
    }
    trackedID = cdata.kameraid;
    var pixel = '<img src="' + piwikbaseurl + 'piwik.php?rec=1&amp;idsite=' + cdata.kameraid;
    pixel += '&amp;cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22' + cdata.kameraid;
    pixel += '%22%5D%2C%5B%22Lang%22%2C%22' + cdata.sprache;
    pixel += '%22%5D%2C%5B%22Portal%22%2C%22' + cdata.portalname;
    pixel += '%22%5D%2C%5B%22Area%22%2C%22' + cdata.area;
    pixel += '%22%5D%2C%5B%22Other%22%2C%22' + cdata.other;
    pixel += '%22%5D%5D&amp;_cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22' + cdata.kameraid;
    pixel += '%22%5D%2C%5B%22Lang%22%2C%22' + cdata.sprache;
    pixel += '%22%5D%2C%5B%22Portal%22%2C%22' + cdata.portalname;
    pixel += '%22%5D%2C%5B%22Area%22%2C%22' + cdata.area;
    pixel += '%22%5D%2C%5B%22Other%22%2C%22' + cdata.other;
    pixel += '%22%5D%5D" style="border:0" alt="" />';
    try {
      var piwikTrackerCam = Piwik.getTracker(piwikbaseurl + 'piwik.php', cdata.kameraid);
      piwikTrackerCam.setCustomVariable(1, 'Cam', cdata.kameraid, 'visit');
      piwikTrackerCam.setCustomVariable(2, 'Lang', cdata.sprache, 'visit');
      piwikTrackerCam.setCustomVariable(3, 'Portal', cdata.portalname, 'visit');
      piwikTrackerCam.setCustomVariable(4, 'Area', cdata.area, 'visit');
      piwikTrackerCam.setCustomVariable(5, 'Other', cdata.other, 'visit');
      piwikTrackerCam.setCustomVariable(1, 'Cam', cdata.kameraid, 'page');
      piwikTrackerCam.setCustomVariable(2, 'Lang', cdata.sprache, 'page');
      piwikTrackerCam.setCustomVariable(3, 'Portal', cdata.portalname, 'page');
      piwikTrackerCam.setCustomVariable(4, 'Area', cdata.area, 'page');
      piwikTrackerCam.setCustomVariable(5, 'Other', cdata.other, 'page');
      piwikTrackerCam.trackPageView();
      piwikTrackerCam.enableLinkTracking();
    } catch (err) {
    }
    if (cdata.kameraid.length > 0) {
      $('.trackPixel').html(pixel);
    }
  }
};
// ANAL
window.trackEvent = function (one, two, three) {
  if (typeof analytics !== 'undefined') {
    analytics.trackEvent(one, two, three, 1);
  }
};
window.trackPage = function (url) {
  if (typeof analytics !== 'undefined') {
    analytics.trackView(url);
  }
};
var portalname = 'APP_feratel.com';
// / ANAL
window.track = function (identify, data, index) {
  if (quickfix[identify] !== undefined) {
    quickfix[identify]();
  }
  var analyze = true;
  if (analyze == true) {
    var camid = '', action = 'home', camname = '', lang = t.pageLang;
    if (identify == 'cam') {
      camid = data.id;
      camname = data.name;
      action = 'streaming';
    }
    var cdata = {
        'kameraid': camid,
        'sprache': lang,
        'portalname': 'APP_feratel.com',
        'area': action,
        'other': trackData.device
      };
    var trackdataName;
    if (data == undefined) {
      data = window.trackData.camdata;
    } else {
      window.trackData.camdata = data;
    }
    if (data.camid == undefined && typeof data.detail !== 'undefined') {
      data.camid = data.detail['110'][index][0];
    }
    // log("track: "+ identify);
    var useanalytics = 0;
    if (typeof analytics !== 'undefined') {
      useanalytics = 1;
    }
    // analytics.trackEvent('Category', 'Action', 'Label', 'de');
    if (identify == 'cam') {
      // load detail page
      // analytics.sendAppView('Streaming Page', successCallback, errorCallback);
      if (useanalytics)
        window.trackPage('Webcam');
      if (useanalytics)
        window.trackEvent('Webcam Ge\xf6ffnet', data.name, camid);
      trackdataName = 'streaming';
    }
    if (identify == 'play') {
      // analytics.sendEvent('Play Webcam', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Play Webcam', data.name, camid);
      trackdataName = 'play_webcam';
    }
    // cam detail
    if (identify == 'historie') {
      // analytics.sendEvent('Detail: Historie', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Historie', data.name, camid);
      trackdataName = 'open_historie';
    }
    if (identify == 'panorama') {
      // analytics.sendEvent('Detail: Panorama', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Panorama', data.name, camid);
      trackdataName = 'open_panorama';
    }
    if (identify == 'wetter_prog') {
      // analytics.sendEvent('Detail: Panorama', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Wetter Prognose', data.name, camid);
      trackdataName = 'wetter_prognose';
    }
    if (identify == 'moreinfo') {
      // analytics.sendEvent('Detail: Weitere Infos', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Weitere Infos', data.name, camid);
      trackdataName = 'open_moreinfo';
    }
    if (identify == 'camsnearcam') {
      // analytics.sendEvent('Detail: Kameras in der Nähe', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Kameras in der N\xe4he', data.name, camid);
      trackdataName = 'open_kamerasindernaehe';
    }
    if (identify == 'share') {
      // analytics.sendEvent('Detail: Share Webcam', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Share Webcam', data.name, camid);
      trackdataName = 'open_share';
    }
    if (identify == 'book') {
      // analytics.sendEvent('Detail: Online Buchen', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Online Buchen', data.name, camid);
      trackdataName = 'open_bookonline';
    }
    if (identify == 'pistenplan') {
      // analytics.sendEvent('Detail: Pistenplan', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Pistenplan', data.name, camid);
      trackdataName = 'open_pistenplan';
    }
    if (identify == 'lifte_pisten') {
      // analytics.sendEvent('Detail: Lifte / Pisten', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Lifte / Pisten', data.name, camid);
      trackdataName = 'open_liftepisten';
    }
    if (identify == 'addtofav') {
      // analytics.sendEvent('Detail: Zu Favoriten Hinzugefügt', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Zu Favorit Hinzugef\xfcgt', data.name, camid);
      trackdataName = 'added_Fav';
    }
    if (identify == 'favremoved') {
      // analytics.sendEvent('Detail: Favorit Entfernt', data.name, camid, lang, successCallback, errorCallback);
      if (useanalytics)
        window.trackEvent('Detail: Favorit Entfernt', data.name, camid);
      trackdataName = 'removed_Fav';
    }
    // pages
    if (identify == 'camsnearme') {
      if (useanalytics)
        window.trackPage('Kameras in meiner N\xe4he');
      trackdataName = 'kameras_in_meiner_naehe';
    }
    if (identify == 'home') {
      if (useanalytics)
        window.trackPage('Home');
      trackdataName = 'home';
    }
    if (identify == 'search') {
      if (useanalytics) {
        // console.log('Test');
        window.trackPage('Webcam Suche');
      }
      // analytics.trackView('Webcam Suche');
      trackdataName = 'search_webcam';
    }
    if (identify == 'menue') {
      if (useanalytics)
        window.trackPage('L\xe4nder Men\xfc');
      trackdataName = 'laender_menue';
    }
    if (identify == 'favorites') {
      if (useanalytics)
        window.trackPage('Favoriten');
      trackdataName = 'favorites';
    }
    if (identify == 'options') {
      if (useanalytics)
        window.trackPage('Mehr');
      trackdataName = 'mehr';
    }
    var trackdata = {
        'kameraid': camid,
        'sprache': lang,
        'portalname': 'APP_feratel.com',
        'area': trackdataName,
        'other': trackData.device
      };
    window.trackPiwik(trackdata);
  }
};
window.addEventListener('load', function () {
  // TRACK
  var device;
  if (iphone) {
    device = 'iPhone';
  }
  if (android) {
    device = 'Android';
  }
  var deviceID = 'TESTID';
  if (typeof window.device !== 'undefined') {
    deviceID = window.device.uuid;
  }
  ;
  // analytics
  _paq = _paq || [];
  (function () {
    var u = piwikbaseurl;
    _paq.push([
      'setSiteId',
      1
    ]);
    _paq.push([
      'setTrackerUrl',
      u + 'piwik.php'
    ]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.defer = true;
    g.async = true;
    g.src = u + 'piwik.js';
    s.parentNode.insertBefore(g, s);
  }());
  var pixel = '<img src="' + piwikbaseurl + 'piwik.php?rec=1&amp;idsite=534';
  pixel += '&amp;cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22';
  pixel += '%22%5D%2C%5B%22Lang%22%2C%22' + t.pageLang;
  pixel += '%22%5D%2C%5B%22Portal%22%2C%22' + portalname;
  pixel += '%22%5D%2C%5B%22Area%22%2C%22' + 'home';
  pixel += '%22%5D%2C%5B%22Other%22%2C%22' + device;
  pixel += '%22%5D%5D&amp;_cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22';
  pixel += '%22%5D%2C%5B%22Lang%22%2C%22' + t.pageLang;
  pixel += '%22%5D%2C%5B%22Portal%22%2C%22' + portalname;
  pixel += '%22%5D%2C%5B%22Area%22%2C%22' + 'home';
  pixel += '%22%5D%2C%5B%22Other%22%2C%22' + device;
  pixel += '%22%5D%5D" style="border:0" alt="" />';
  // console.log(pixel);
  try {
    var piwikTrackerCam = Piwik.getTracker(piwikbaseurl + 'piwik.php', 534);
    piwikTrackerCam.setCustomVariable(1, 'Cam', '', 'visit');
    piwikTrackerCam.setCustomVariable(2, 'Lang', t.pageLang, 'visit');
    piwikTrackerCam.setCustomVariable(3, 'Portal', portalname, 'visit');
    piwikTrackerCam.setCustomVariable(4, 'Area', 'home', 'visit');
    piwikTrackerCam.setCustomVariable(5, 'Other', device, 'visit');
    piwikTrackerCam.setCustomVariable(1, 'Cam', '', 'page');
    piwikTrackerCam.setCustomVariable(2, 'Lang', t.pageLang, 'page');
    piwikTrackerCam.setCustomVariable(3, 'Portal', portalname, 'page');
    piwikTrackerCam.setCustomVariable(4, 'Area', 'home', 'page');
    piwikTrackerCam.setCustomVariable(5, 'Other', device, 'page');
    piwikTrackerCam.trackPageView();
    piwikTrackerCam.enableLinkTracking();
  } catch (err) {
  }
  // // $('.trackPixel').html(pixel);
  $('.staticTrackPixel').html(pixel);
  // // / TRACK
  setInterval(function () {
    window.checkOrientation();
  }, 3000);
  // setTimeout(function() {
  // $('.container').fadeIn();
  $('body').addClass('opacityf');
  // }, 800);
  function updateOnlineStatus(event) {
    var condition = navigator.onLine ? 'online' : 'offline';  // alert(condition);
  }
  window.addEventListener('online', updateOnlineStatus);
  window.addEventListener('offline', updateOnlineStatus);
  updateOnlineStatus();
});
var scrollto = function (y, t) {
  if (typeof y == 'string') {
    var yO = $(y).offset().top;
    var yH = $(y).height();
    $('.mb-page').scrollTo(yO - yH - 15, t);
  } else {
    $('.mb-page').scrollTo(y, t);
  }
};
var editData = function (data) {
  jsonOrte = data.orte;
  for (var i = jsonOrte.length - 1; i >= 0; i--) {
    if (jsonOrte[i]['detail']['110'].length > 0) {
      jsonOrte[i]['detail']['hasCams'] = 'true';
    } else {
      // jsonOrte[i]['detail']['hasCams'] = 'false';
      jsonOrte.splice(i, 1);
    }
  }
  jsonCams = data.cams;
  // console.log(jsonCams);
  jsonMenu = data.menu;  // console.log(jsonMenu);
};
var error = function (e) {
};
var loadXMLdata = function (scope, id, res) {
  // window.location.href="#/";
  if (jsonLoading) {
    return false;
  }
  ;
  setTimeout(function () {
    if (jsonOrte === undefined) {
      loadingOVL(1);
      jsonLoading = 1;
      console.log(t.pageLang);
      var conFin = 0;
      // console.log('http://www.appicc.com/feratel/app/suche_n.php?lang=' + t.pageLang+'&test=test');
      var con = $.getJSON('http://www.appicc.com/feratel/app/suche_n.php?lang=' + t.pageLang + '').done(function (data) {
          console.log(data);
          if (!data.exception) {
            conFin = 1;
            delete localStorage.jsonOrte;
            loadingOVL(0);
            localStorage.lastUpdate = new Date().getTime();
            localStorage.jsonOrte = JSON.stringify(data);
            jsonLoading = 0;
            editData(data);
            if (scope === undefined) {
              var scope = angular.element($('.Page')[0]).scope();
              scope.$apply(function () {
                scope.status = 'fertig';
                scope.ortMenuList = jsonOrte;
                scope.ortMenu = {};
                scope.ortMenu.lvl0 = jsonMenu;
                if (scope.cam == undefined) {
                  scope.cam = [];
                }
                scope.cam.loading = undefined;
              });
            }
            ;
            if (id == 'suche' && scope !== undefined) {
              scope.$apply(function () {
                scope.status = 'fertig';
                scope.ortMenuList = jsonOrte;
              });
            }
            if (id == 'menu' && scope !== undefined) {
              scope.$apply(function () {
                scope.status = 'fertig';
                scope.ortMenu.lvl0 = jsonMenu;
              });
            }
          } else {
          }
          redoFavorites();
          if (res != undefined) {
            res();
          }
        }).fail(function (data) {
          customAlert(1, 'con');
          $('.loadingOLV').css('display', 'none');
          jsonLoading = 0;
          conFin = 1;
        });
      setTimeout(function () {
        if (conFin == 0) {
          customAlert(1, 'con');
          $('.loadingOLV').css('display', 'none');
          jsonLoading = 0;
        }
      }, 40000);
    } else {
      if (id == 'suche') {
        // scope.$apply(function() {
        scope.status = 'fertig';
        scope.ortMenuList = jsonOrte;  // });
      }
      if (id == 'menu') {
        // scope.$apply(function() {
        scope.status = 'fertig';
        scope.ortMenu.lvl0 = jsonMenu;  // });
      }
    }
  }, 1);
};
var updateXMLdata = function (res) {
  jsonOrte = undefined;
  jsonMenu = undefined;
  delete localStorage.jsonOrte;
  delete localStorage.jsonMenu;
  loadXMLdata(0, 0, res);  // alert('Update');
};
var nowD = new Date().getTime();
if (localStorage.lastUpdate !== undefined) {
  var lastUpdate = nowD - localStorage.lastUpdate;
  if (lastUpdate > 86400000 * 2) {
    updateXMLdata();
  }
  ;
}
if (window.outerHeight > window.outerWidth) {
  vertical = 1;
  horizontal = 0;
} else {
  vertical = 0;
  horizontal = 1;
}
if (android) {
  $('body').addClass('androidAPP');
} else {
  $('body').addClass('iosAPP');
}
if (window.outerHeight > 500 && window.outerWidth > 400 && ipad) {
  $('body').addClass('ipadAPP');
  $('.corner').attr('height', '95').find('polygon').attr('points', '0,0 0,70 260,95 320,75 320,0 0,0');
  ipad = true;
}
// var loadjsonOrte = function() {
//   jsonLoading=1;
//   $.getJSON('http://www.appicc.com/feratel/app/suche.php', function(e) {
//     if (!e.exception) {
//       localStorage.jsonOrte = JSON.stringify(e);
//       editData(e);
//       jsonLoading=0;
//     }else{
//       error(e);
//     };
//   });
// };
if (localStorage.jsonOrte !== undefined) {
  var data = jQuery.parseJSON(localStorage.jsonOrte);
  // console.log(JSON.parse(localStorage.jsonOrte));
  editData(data);
} else {
}
if (localStorage.jsonOrte === undefined) {
  setTimeout(function () {
    loadXMLdata();
  }, 400);
}
// if (localStorage. !== undefined) {
// };
if (localStorage.camFavorites == undefined) {
  localStorage.camFavorites = JSON.stringify([]);
}
var langsPages = [
    'impressum',
    'sprache'
  ];
var langs = [
    'en',
    'de',
    'sk',
    'cz',
    'it',
    'fr'
  ];
var app = angular.module('FeratelApp', ['ajoslin.mobile-navigate']);
app.config([
  '$locationProvider',
  '$routeProvider',
  function ($locationProvider, $routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    }).when('/main.html', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    }).when('/menu.html', {
      templateUrl: 'views/menu.html',
      controller: 'OrtMenu'
    }).when('/favoriten.html', {
      templateUrl: 'views/favoriten.html',
      controller: 'FavCtrl'
    }).when('/options.html', {
      templateUrl: 'views/options.html',
      controller: 'OptCtrl'
    }).when('/suche.html', {
      templateUrl: 'views/suche.html',
      controller: 'MainCtrl'
    }).when('/suchemap.html', {
      templateUrl: 'views/suchemap.html',
      controller: 'mapCtrl'
    }).when('/detail.html', {
      templateUrl: 'views/detail.html',
      controller: 'DetailCtrl'
    });
    for (var i = 0; i < langsPages.length; i++) {
      for (var a = 0; a < langs.length; a++) {
        $routeProvider.when('/' + langs[a] + '/' + langsPages[i] + '.html', {
          templateUrl: 'views/content/' + langs[a] + '/' + langsPages[i] + '.html',
          controller: 'MainCtrl'
        });
      }
      ;
    }
    ;  // $locationProvider.html5Mode(true);
       // $locationProvider.hashPrefix('!');
  }
]);
// SET BG TRANSPARENCY
var defaultbg = false;
function toggleBG(set, option) {
  if (option === false || option === true) {
    defaultbg = option;
  }
  if (set != undefined) {
    switch (set) {
    case 'true':
      $('.whiteCover').removeClass('off').removeClass('double');
      break;
    case 'double':
      $('.whiteCover').removeClass('off').addClass('double');
      break;
    default:
      if (!defaultbg) {
        $('.whiteCover').addClass('off').removeClass('double');
      }
    }
  } else {
    $('.whiteCover').toggleClass('off');
  }
}
// SEARCH LAYOUT
function moveBody(set) {
  if (set != undefined) {
    switch (set) {
    case 'true':
      $('body').addClass('andInput');
      $('.footer').css('display', 'none');
      // $('.Page h2').slideUp(100, function(){
      //   $('input').focus();
      // });
      break;
    default:
      $('body').removeClass('andInput');
      $('.footer').css('display', 'block');  // $('.Page h2').slideDown();
    }
  } else {
    $('body').toggleClass('andInput');
    $('.footer').removeAttr('style');  // $('.Page h2').toggleSlide();
  }
}
;
function toggleSearch(set) {
  if (set != undefined) {
    switch (set) {
    case 'true':
      $('body').addClass('searchform');
      break;
    default:
      $('body').removeClass('searchform');
    }
  } else {
    $('body').toggleClass('searchform');
  }
}
;
function loadingOVL(state) {
  if (state === 1) {
    $('.loadingOLV').fadeIn(200);
  } else {
    $('.loadingOLV').fadeOut(200);
  }
}
function cAlert(t, y, n) {
}
function eliminateDuplicates(arr) {
  var i, len = arr.length, out = [], obj = {};
  for (i = 0; i < len; i++) {
    obj[arr[i]] = 0;
  }
  for (i in obj) {
    out.push(i);
  }
  return out;
}
function redoFavorites() {
  if (localStorage.camFavorites != undefined && jsonOrte != undefined) {
    var favs = jQuery.parseJSON(localStorage.camFavorites);
    var fnames = [];
    var nfavs = [];
    for (var i = 0; i < favs.length; i++) {
      fnames.push(favs[i].id);  // console.log(favs[i].id);
    }
    ;
    // console.log(fnames);
    fnames = eliminateDuplicates(fnames);
    // console.log(fnames);
    // console.log(favs[0].name);
    for (var i = jsonOrte.length - 1; i >= 0; i--) {
      for (var e = 0; e < fnames.length; e++) {
        if (fnames[e] == jsonOrte[i].id) {
          nfavs.push(jsonOrte[i]);
        }
        ;
      }
      ;
    }
    ;
    // restore original order
    var returnFavs = [];
    for (var i = 0; i < favs.length; i++) {
      for (var l = 0; l < nfavs.length; l++) {
        if (favs[i].id == nfavs[l].id) {
          // console.log(nfavs[l].id, favs[0].id);
          returnFavs.push(nfavs[l]);
        }
      }
      ;
    }
    ;
    // console.log(nfavs);
    // console.log("new favs: ", nfavs);
    localStorage.camFavorites = JSON.stringify(returnFavs);
  }
}
app.directive('ngTap', [
  '$navigate',
  function ($navigate) {
    return function (scope, element, attrs) {
      var tapping;
      var tapX, tapY;
      tapping = false;
      element.bind('touchstart', function (e) {
        tapping = true;
        tapX = e.originalEvent.touches[0].pageX;
        tapY = e.originalEvent.touches[0].pageY;
        element.addClass('active');
      });
      element.bind('touchmove', function (e) {
        var xPos = e.originalEvent.touches[0].pageX;
        var yPos = e.originalEvent.touches[0].pageY;
        var xdiff = xPos - tapX;
        var ydiff = yPos - tapY;
        // if (xdiff > 5 || ydiff > 5) {
        tapping = false;
        // }
        element.removeClass('active');
      });
      element.bind('touchend', function () {
        toggleSearch('false');
        element.removeClass('active');
        if (tapping) {
          if (attrs['ngTap'].indexOf('.html') !== -1) {
            // Normal Menu Redirect
            scope.$apply($navigate.go('/' + attrs['ngTap']));
          } else {
            var setCamDetail = function (id) {
              localStorage.fromMenu = false;
              // console.log(typeof id);
              if (typeof id == 'object') {
                camDetailIndex = id;
                localStorage.camDetailIndex = JSON.stringify(id);
              }
              if (typeof id == 'number') {
                var favs = JSON.parse(localStorage.camFavorites);
                camDetailIndex = favs[id];
                localStorage.camDetailIndex = JSON.stringify(favs[id]);
              }
              // Go To Cam Detail
              scope.$apply($navigate.go('/detail.html'));
            };
            var goToC = function (file) {
              var scope = angular.element($('.Page')[0]).scope();
              scope.$apply($navigate.go('/' + t.pageLang + '/' + file + '.html'));
            };
            var goBack = function () {
              // Go To Cam Detail
              scope.$apply($navigate.back());
              return false;
            };
            if (android) {
              moveBody('false');
            }
            ;
            var fncName = attrs.ngTap.split('(')[0];
            if (window[fncName] == undefined) {
              scope.$apply(attrs['ngTap']);
            } else {
              eval(attrs['ngTap']);
            }
          }
          return false;
        }
      });
    };
  }
]).directive('ngBg', [function () {
    // Runs during compile
    return {
      restrict: 'A',
      link: function ($scope, iElm, iAttrs, controller) {
        var dbg = false;
        if (iAttrs.ngBg === 'true') {
          dbg = true;
        }
        if (iAttrs.ngBg === 'double') {
        }
        ;
        toggleBG(iAttrs.ngBg, dbg);
      }
    };
  }]).directive('ngReponsivesvg', function () {
  return function (scope, element, attrs) {
    var responsiveHeader = function () {
      var wW = $(window).width();
      var data = attrs.points;
      data = data.split(' ');
      for (var i = 0; i < data.length; i++) {
        data[i] = data[i].split(',');
        for (var p = 0; p < data[i].length; p++) {
          if (p == 0) {
            data[i][p] = wW * (data[i][p] / 320);
          }
        }
      }
      var string = '';
      for (var i = 0; i < data.length; i++) {
        for (var p = 0; p < data[i].length; p++) {
          string += data[i][p];
          if (p == 0) {
            string += ',';
          }
        }
        if (i - 1 != data.length) {
          string += ' ';
        }
      }
      element.attr('points', string);
      window.checkOrientation();
      setTimeout(window.saveWidth, 10);
    };
    responsiveHeader();
    window.onresize = function (event) {
      responsiveHeader();
    };
  };
}).directive('ngPagem', function () {
  return function (scope, element, attrs) {
    $('.mainO').removeClass('menuAct');
    $('.' + attrs.ngPagem).addClass('menuAct');
  };
}).directive('searchform', [function () {
    return {
      restrict: 'A',
      link: function ($scope, iElm, iAttrs, controller) {
        var tapped = false;
        var input = iElm.find('input');
        var lupe = iElm.find('.sic');
        var $search = input, searchBlurable = false;
        lupe.on('click', function () {
          scrollto(0, 1);
          input.focus();
        });
        input.on('touchstart', function (e) {
          tapped = true;
        }).on('touchmove', function () {
          tapped = false;
        }).on('touchend', function (e) {
          if (tapped) {
            scrollto(0, 1);
            toggleBG('true');
            if (iphone || ipad) {
              toggleSearch('true');
              $search.click().focus();
            }
            if (android) {
              moveBody('true');
              input.focus();
              return false;
            }
            ;
          }
        }).on('blur', function (e) {
          if (iphone || ipad) {
            toggleSearch('false');
          }
          if (android) {
            moveBody('false');
          }
          ;
          toggleBG('false', 0);
        }).on('focus', function () {
          scrollto(0, 1);
          toggleBG('true');
          if (iphone || ipad) {
            toggleSearch('true');
          }
          if (android) {
            moveBody('true');
          }
          ;
        });
      }
    };
  }]).directive('backbutton', [function () {
    return {
      restrict: 'E',
      template: '<a href=# class="backbutton" ng-tap="goBack()"><i class="bic"></i> {{t.back}}</a>',
      link: function ($scope, iElm, iAttrs, controller) {
        if (iAttrs.blue != undefined) {
          iElm.find('.backbutton').addClass('blueB');
        }
      }
    };
  }]).directive('loader', [function () {
    // Runs during compile
    return {
      restrict: 'E',
      template: '<div class="loadingCircle">loading<i class="ani"></i></div>',
      link: function ($scope, iElm, iAttrs, controller) {
      }
    };
  }]).directive('favorite', [function () {
    return {
      restrict: 'E',
      template: '<div class="favStar"></div>',
      link: function ($scope, iElm, iAttrs, controller) {
        var detail = jQuery.parseJSON(localStorage.camDetailIndex), id, name, push, index, t = 0, favs = [], isFav = 0, star = iElm.find('.favStar');
        if (localStorage.camFavorites !== undefined) {
          favs = jQuery.parseJSON(localStorage.camFavorites);
        }
        ;
        setTimeout(function () {
          id = detail.id;
          push = detail;
          for (var i = favs.length - 1; i >= 0; i--) {
            if (favs[i].id == id) {
              isFav = 1;
              index = i;
              $scope.$apply(function () {
                $scope.isFav = 'isFavStar';
              });
            }
          }
        }, 1);
        iElm.on('touchstart', function () {
          t = 1;
        }).on('touchmove', function () {
          t = 0;
        }).on('touchend', function () {
          // isFav = 0;
          // console.log($scope);
          if (t) {
            if (!isFav) {
              // add to favorites
              window.track('addtofav');
              // console.log(push);
              push = { id: push.id };
              // console.log(push);
              favs.push(push);
              localStorage.camFavorites = JSON.stringify(favs);
              isFav = 1;
              $scope.$apply(function () {
                $scope.isFav = 'isFavStar';
              });
              miniAlert($scope.t.addToFav);
            } else {
              // remove from favorites
              window.track('favremoved');
              favs.splice(index, 1);
              localStorage.camFavorites = JSON.stringify(favs);
              isFav = 0;
              $scope.$apply(function () {
                $scope.isFav = '';
              });
              miniAlert($scope.t.removeFromFav);
            }
            window.setFavCount();
            star.toggleClass('active');
          }
        });
      }
    };
  }]).directive('ngCamslideshow', function ($navigate, $route) {
  // Runs during compile
  return {
    restrict: 'A',
    link: function ($scope, iElm, iAttrs, controller) {
      var okSlides = ['0'];
      var cams = $scope.cams;
      function loadContent(index, elem) {
        globalSlideIndex = index;
        $('video').each(function () {
          $(this)[0].pause();
        });
        loadContentT = new Date().getTime();
        var obj = $('.slide').first().find('.iframe');
        if (elem != undefined) {
          obj = $(elem).find('.iframe');
        }
        if (obj.attr('src') === undefined) {
          if ($scope.cams === undefined) {
            $scope.$apply($navigate.go('/suche.html'));
          }
          if (loadedSlides['_' + index] !== 'loaded') {
            var camdetail = jQuery.parseJSON(localStorage.camDetailIndex);
            // window.trackCam(jQuery.parseJSON(localStorage.camDetailIndex), index);
            window.track('cam', camdetail, index);
            trackCam();
            obj.attr('src', $scope.cams[index].urllist.durl['9'].data.v);
            $scope.$apply(function () {
              $scope.cams[index].hasVideo = 0;
              $scope.cams[index].slideIndex = index;
              $scope.cams[index].displayOptions = 0;
              $scope.cams[index].videoLoading = 0;
            });
            // load extra data
            var geturl = 'http://appicc.com/feratel/app/dev_info.php?lang=' + t.pageLang + '&info=';
            geturl += encodeURIComponent($scope.cams[index].data.info);
            // console.log(geturl);
            $scope.$apply(function () {
              $scope.cams[index].loading = 'loading';
            });
            $.getJSON(geturl, function (e) {
              // console.log(e);
              // console.log(e);
              if (e.code) {
              }
              ;
              loadedSlides['_' + index] = 'loaded';
              // var ortid = e.rid;
              // setPageHeight('detailPage', '.sl_0');
              pistenplanUrl = e.pistenplan;
              $scope.$apply(function () {
                // $scope.rid = ortid;
                $scope.cams[index].videoSD = e.video_sd;
                $scope.cams[index].videoHD = e.video_hd;
                $scope.cams[index].pvImage = e.image;
                $scope.cams[index].displayOptions = 1;
                $scope.cams[index].videoLoading = 1;
                $scope.cams[index].playClasses = '';
                if ($scope.cams[index].videoHD !== null && $scope.cams[index].videoHD !== 'null') {
                  $scope.cams[index].playClasses = 'ld';
                } else {
                  // $('.playVideo').addClass('noplay');
                  $scope.cams[index].playClasses = 'noplay';
                }
                ;
                if (e.video_hd !== null && e.video_hd !== 'null') {
                  $scope.cams[index].hasVideo = 1;
                }
                $scope.cams[index].video = document.getElementById('video_' + index);
                var vidEl = document.getElementById('video_' + index);
                if (vidEl !== null && vidEl !== undefined) {
                  vidEl.volume = 0;
                  $(vidEl).on('loadstart', function () {
                    $scope.$apply(function () {
                      $scope.cams[index].displayOptions = 1;
                      $scope.cams[index].videoLoading = 1;
                    });
                  }).on('play', function () {
                    $scope.$apply(function () {
                      $scope.cams[index].displayOptions = 0;
                    });
                  }).on('loadeddata', function () {
                    $scope.$apply(function () {
                      $scope.cams[index].videoLoading = 0;
                    });
                  }).on('pause', function () {
                    $scope.$apply(function () {
                      $scope.cams[index].displayOptions = 1;
                    });
                  });
                  document.getElementById('video_' + index).onplay = function () {
                  };
                } else {
                }
                var bInfo = {};
                if (e.info !== undefined) {
                  for (var i = e.info.length - 1; i >= 0; i--) {
                    switch (e.info[i].t) {
                    case '0':
                      bInfo.ort = e.info[i].v;
                      break;
                    case '3':
                      bInfo.standort = e.info[i].v;
                      break;
                    case '4':
                      bInfo.kanton = e.info[i].v;
                      break;
                    case '5':
                      bInfo.land = e.info[i].v;
                      // t.kanton = 'Kanton';
                      // t.bundesland = 'Bundesland';
                      // t.provinz = 'Provinz';
                      // t.staat = 'Staat';
                      var landCode = e.info[i].k;
                      if (landCode == 'CHE') {
                        $scope.cams[index].stupidtext = t.kanton;
                      } else if (landCode == 'AUT' || landCode == 'DEU') {
                        $scope.cams[index].stupidtext = t.bundesland;
                      } else if (landCode == 'USA') {
                        $scope.cams[index].stupidtext = t.staat;
                      } else {
                        $scope.cams[index].stupidtext = t.provinz;
                      }
                      break;
                    case '6':
                      bInfo.lat = e.info[i].v;
                      break;
                    case '7':
                      bInfo.lng = e.info[i].v;
                      break;
                    }
                  }
                }
                ;
                if (e.all !== undefined) {
                  var hasMoreInfo = false;
                  for (var i = e.all.length - 1; i >= 0; i--) {
                    if (e.all[i].data.t == '5') {
                      hasMoreInfo = true;
                    }
                  }
                }
                ;
                if (!hasMoreInfo) {
                  var dMi = {
                      data: {
                        c: t.moreinfo,
                        cn: 'buttonid_5',
                        rid: '4026',
                        s: '1',
                        t: '5'
                      },
                      pci: {
                        t: '0',
                        v: '0'
                      }
                    };
                  if (typeof e.all == 'object' || typeof e.all == 'array') {
                    e.all.push(dMi);
                  }
                  ;
                }
                var hasLocID = false;
                // if (jQuery.parseJSON(localStorage.camDetailIndex).locid != undefined) {
                var caminfo = jQuery.parseJSON(localStorage.camDetailIndex);
                // console.log(caminfo);
                hasLocID = jQuery.parseJSON(localStorage.camDetailIndex).locid;
                // }
                // if (hasLocID !== false) {
                //   hasLocID = 0;
                // };
                // console.log(hasLocID);
                if (hasLocID !== undefined && hasLocID.length !== 0) {
                  var oplineBuchen = {
                      data: {
                        c: t.onlineBuchen,
                        cn: 'cs_button_3',
                        rid: '0',
                        s: '0',
                        t: '91'
                      },
                      pci: {
                        t: '0',
                        v: hasLocID
                      }
                    };
                  e.all.push(oplineBuchen);
                }
                ;
                if (typeof e.pistenplan == 'string') {
                  var pistenplanButton = {
                      data: {
                        c: t.pistenplan,
                        cn: 'cs_button_88',
                        rid: '0',
                        s: '0',
                        t: '92'
                      },
                      pci: {
                        t: '0',
                        v: '0'
                      }
                    };
                  e.all.push(pistenplanButton);
                }
                ;
                var camNear = {
                    data: {
                      c: t.camNearCam,
                      cn: 'cs_button_1',
                      rid: '0',
                      s: '1',
                      t: '94'
                    },
                    pci: {
                      t: '0',
                      v: '0'
                    }
                  };
                var camShare = {
                    data: {
                      c: t.camShareT,
                      cn: 'cs_button_2',
                      rid: '0',
                      s: '0',
                      t: '95'
                    },
                    pci: {
                      t: '0',
                      v: '0'
                    }
                  };
                // console.log(typeof e.all);
                if (typeof e.all == 'object' || typeof e.all == 'array') {
                  e.all.push(camNear, camShare);
                }
                $scope.cams[index].basicInfo = bInfo;
                // console.log(e.all);
                $scope.cams[index].info = e.all;
                $scope.cams[index].loading = undefined;
                if (e.wetter !== null) {
                  var weather = e.wetter;
                  var extraWeather, weatherForcast, weatherMore;
                  if (weather === undefined) {
                  }
                  if (weather.length == 5) {
                    extraWeather = weather[0];
                    weather.splice(0, 1);
                  }
                  var wBox = [];
                  // BOX
                  wBox.now = weather[0].wi[0].wid[1].v + '\xb0';
                  if (weather[1]) {
                    wBox.state = weather[1].wi[0].wid[5].v;
                  }
                  var wDetail = [];
                  if (weather.length > 1) {
                    wDetail.days = weather[1].wi;
                  }
                  var todayDetail = [];
                  if (weather[3]) {
                    todayDetail = weather[3];
                    todayDetail.wi.splice(2, 1);
                    if (parseInt(todayDetail.wi[0].wid[1].v) > 0) {
                      todayDetail.wi[0].wid[1].v = todayDetail.wi[0].wid[1].v + ' cm';
                    }
                    // console.log();
                    if (parseInt(todayDetail.wi[1].wid[1].v) > 0) {
                      todayDetail.wi[1].wid[1].v = todayDetail.wi[1].wid[1].v + ' cm';
                    }
                    var lastSnow, lSi;
                    for (var i = todayDetail.wi.length - 1; i >= 0; i--) {
                      if (todayDetail.wi[i].data.t == '29') {
                        var lastSnow = new Date(todayDetail.wi[i].wid[1].v);
                        lSi = i;
                      }
                    }
                    if (lSi !== undefined) {
                      todayDetail.wi[lSi].wid[1].v = lastSnow.getDate() + '.' + (lastSnow.getMonth() + 1) + '.' + lastSnow.getFullYear();
                    }
                  }
                  // todayDetail.days = weather[1].wi;
                  // weatherForcast = e.wetter[0];
                  // weatherMore = e.wetter[2];
                  // if (weatherMore !== undefined) {
                  //   if ((weatherMore.wi).length > 4) {
                  //     var lastSnow = new Date(weatherMore.wi[4].wid[1].v);
                  //     weatherMore.wi[4].wid[1].v = (lastSnow.getDate() + '.' + (lastSnow.getMonth()+1) + '.' + lastSnow.getFullYear());
                  //   };
                  //   // weatherToday
                  //   weatherMore.wi.splice(2,1);
                  //   weatherMore.wi[0].wid[1].v = weatherMore.wi[0].wid[1].v+' cm';
                  //   weatherMore.wi[1].wid[1].v = weatherMore.wi[1].wid[1].v+' cm';
                  // };
                  // $scope.cams[index].weatherToday = weatherForcast;
                  $scope.cams[index].weatherBox = wBox;
                  $scope.cams[index].weatherExtra = todayDetail;
                  $scope.cams[index].weatherDetail = wDetail;
                }
                // Info Buttons
                $scope.loadExtraInfo = function (tID) {
                  index = globalSlideIndex;
                  $('.Page').addClass('extrainfo');
                  var tData, url, what, fromView, toView;
                  for (var i = 0; i < $scope.cams[index].info.length; i++) {
                    if ($scope.cams[index].info[i].data.t === tID + '') {
                      tData = $scope.cams[index].info[i];
                      url = tData.pci.v;
                      what = tData.data.cn;
                      fromView = 'ev_' + index;
                      toView = 'nv_' + index;
                    }
                    ;
                  }
                  ;
                  var loadExtraUrl = 'http://appicc.com/feratel/app/load_buttons.php?lang=' + t.pageLang + '&w=' + what + '&l=' + encodeURIComponent(url);
                  // console.log(loadExtraUrl);
                  $('.weitere_infos').css('display', 'none');
                  if (extraInfoCache !== undefined) {
                    $scope.cams[index][extraInfoCache] = '';
                  }
                  $scope.cams[index].loading = 'loading';
                  if (what === 'buttonid_9') {
                    $scope.fatBoxIt(t.panoBlick, '0');
                  }
                  if (what === 'cs_button_88') {
                    window.track('pistenplan');
                    $scope.fatBoxIt(t.pistenplan, pistenplanUrl, 'pano');
                    $scope.cams[index].loading = undefined;
                  }
                  if (what !== 'buttonid_9' && what !== 'skimap' && what !== 'buttonid_8' && what !== 'cs_button_1' && what !== 'cs_button_2' && what !== 'cs_button_88') {
                    $scope.show_view(fromView, toView);
                  }
                  if (url == '0') {
                    loadExtraUrl = '#';
                  }
                  ;
                  setTimeout(function () {
                    $.getJSON(loadExtraUrl, function (e) {
                      // console.log(e);
                      // setTimeout(function(){
                      // var nvH = $('.'+fromView).height();
                      // setPageHeight(750);
                      // },50);
                      var seDetailHeight = function () {
                        pageScroll(0);
                        var ot = $('.loadExtraInfo').offset().top;
                        var wh = $(window).height();
                        var fh = $('.footer').height();
                        $('.loadExtraInfo').height(wh - ot - fh);
                      };
                      // if (android) {
                      seDetailHeight();
                      $(window).on('resize', seDetailHeight);
                      // }
                      $scope.$apply(function () {
                        $scope.cams[index].loading = undefined;
                        if (what == 'buttonid_9') {
                          window.track('panorama');
                          $scope.fatBoxIt(t.panoBlick, e.buttonid_9, 'pano', 'scroll');
                        }
                        if (what == 'buttonid_2') {
                          window.track('historie');
                          e[what] = e[what].reverse();
                          for (var i = e[what].length - 1; i >= 0; i--) {
                            // e[what][i].de = new Date(e[what][i].d);
                            var dd = new Date(e[what][i].d);
                            var hours = dd.getHours();
                            if (hours > 24) {
                              hours -= 24;
                            } else if (hours === 0) {
                              hours = 24;
                            }
                            var minute = dd.getMinutes();
                            if (minute.toString().length == 1) {
                              minute = '0' + minute;
                            }
                            //
                            e[what][i].dnd = dd.getDate() + '.' + (dd.getMonth() + 1) + '.' + dd.getFullYear() + ' - ' + (hours - 2) + ':' + minute;
                            e[what][i].d = hours - 2 + ':' + minute;
                          }
                        }
                        if (what == 'skimap') {
                          openLink(e.skimap);  // return false
                        }
                        if (what == 'buttonid_5') {
                          window.track('moreinfo');
                          var ch = e[what].lsi;
                          for (var i = ch.length - 1; i >= 0; i--) {
                            if (ch[i].ll == 'Hutchison') {
                              e[what].lsi.splice(i, 1);
                            }
                            ;
                          }
                          ;
                        }
                        if (what == 'buttonid_4') {
                          // canSwipe=true;
                          window.track('lifte_pisten');
                          if (mySwipe !== undefined) {
                            mySwipe.suspend();
                          }
                          ;
                          setTimeout(function () {
                            var slider = $('.' + fromView + ' .slideContainer');
                            $('.' + fromView + ' .titles span').removeClass('active');
                            $('.' + fromView + ' .titles .t_' + 0).addClass('active');
                            // disable scrolling
                            pageScroll(0);
                            var $sizeel = $('.lifte_pisten .slidePadder');
                            var fromTop = $sizeel.offset().top;
                            var fromBottom = 70;
                            var sizeelHeight = $(window).height() - fromTop - fromBottom;
                            $sizeel.css('height', sizeelHeight);
                            window.pistenSwipe = new Swipe(slider[0], {
                              speed: 300,
                              auto: false,
                              continuous: true,
                              disableScroll: false,
                              stopPropagation: false,
                              callback: function (index, elem) {
                                // setPageHeight('detailPage', '.sl_'+index);
                                $('.' + fromView + ' .titles span').removeClass('active');
                                $('.' + fromView + ' .titles .t_' + index).addClass('active');  // inicBox.removeClass('init');
                                                                                                // inicBox.find('i').removeClass('a');
                                                                                                // inicBox.find('.i' + index).addClass('a');
                                                                                                // if (mySwipe.getPos() == mySwipe.getNumSlides()) {
                                                                                                //   mySwipe.slide(0, 0);
                                                                                                // }
                                                                                                // if (mySwipe.getPos() > mySwipe.getNumSlides()) {
                                                                                                //   mySwipe.slide(mySwipe.getNumSlides() - 1, 0);
                                                                                                // }
                              },
                              transitionEnd: function (index, elem) {
                              }
                            });
                            $scope.$apply(function () {
                              $scope.gotoSlide = function (id) {
                                pistenSwipe.slide(parseInt(id), 300);
                              };
                            });
                          });
                        }
                        $scope.cams[index][what] = e[what];
                        extraInfoCache = what;
                      });
                    }).fail(function (e) {
                      if (url.indexOf('http') === -1) {
                        if (what == 'buttonid_5') {
                          window.track('moreinfo');
                          $scope.$apply(function () {
                            $scope.cams[index].loading = undefined;
                            $('.weitere_infos').css('display', 'block');
                          });
                        }
                        if (what == 'cs_button_3') {
                          $scope.$apply(function () {
                            window.track('book');
                            $scope.cams[index].loading = undefined;
                            $('.buchen_form').css('display', 'block');
                            bmForm($('.buchen_form_box'));
                            $('.bookNow').unbind('click');
                            $('.bookNow').on('click', function () {
                              var date = $('#anreise').val(), nights = $('#nights').val(), adults = $('#adults').val(), children = $('#children').val();
                              var childrenAge = '';
                              for (var i = 0; i < children; i++) {
                                childrenAge += ',' + $('.childage .part-' + (i + 1) + ' input').val();
                              }
                              childrenAge = childrenAge.replace(',', '');
                              date = date.replace(/-/g, '');
                              var link = 'http://web4.deskline.net/feratelcom/' + localStorage.lang + '/accommodation/searchresult?lkRG=TO&lkID=' + url + '&selArrivalDate=' + date + '&selNights=' + nights + '&selAdultsSearchLine1=' + adults;
                              var link = 'http://web4.deskline.net/feratelcom/' + localStorage.lang + '/accommodation/qfind?lkRG=TO&lkID=' + url + '&selNights=' + nights + '&selArrivalDate=' + date + '&selNumberOfUnits=1&selAdultsSearchLine1=' + adults;
                              if (childrenAge) {
                                link += '&selChildrenSearchLine1=' + childrenAge;
                              } else {
                              }
                              openLink(link);
                            });
                            $('#children').on('change', function () {
                              var children = $(this).val();
                              var ch = parseInt(children);
                              $('.childage').css('display', 'none');
                              if (ch > 0) {
                                $('.childage').css('display', 'block');
                                $('.childage .split').removeClass('split-9').addClass('split-2').css('display', 'none');
                                for (var i = 1; i <= ch; i++) {
                                  $('.childage .part-' + i).css('display', 'block');
                                }
                              }  // for (var i = 1; i <= ch; i++) {
                                 //     $('.buchen_form_box .split-'+i).css('display', 'block');
                                 // };
                            });
                          });  // alert('form data');
                               // openLink('http://web4.deskline.net/feratelcom/' + localStorage.lang + '/accommodation/searchresult?lkRG=TO&lkID=' + url);
                               // $scope.$apply(function() {
                               //     $scope.cams[index].loading = undefined;
                               // });
                               // $scope.$apply(function() {
                               //     $scope.cams[index].loading = undefined;
                               //     $('.online_buchen_form').css('display', 'block');
                               // });
                        }
                        if (what == 'cs_button_1') {
                          window.track('camsnearcam');
                          $scope.cams[index].loading = undefined;
                          fromDetailMapLink = 1;
                          camsNearByCenter = jQuery.parseJSON(localStorage.camDetailIndex).geo;
                          $scope.$apply($navigate.go('/suchemap.html'));  // mmaps
                        }
                        if (what == 'cs_button_2') {
                          window.track('share');
                          var camI = $scope.cams[index];
                          var shareImage = camI.pvImage;
                          var shareTitle = camI.basicInfo.ort + ' - ' + camI.data.l + ' | feratel.com Webcam';
                          window.plugins.socialsharing.share(shareTitle, null, shareImage);
                          $scope.$apply(function () {
                            $scope.cams[index].loading = undefined;
                          });
                        }
                      } else {
                      }
                      ;
                    });
                  }, 50);
                };
              });
            }).fail(function (e) {
              $scope.$apply(function () {
                $scope.cams[index].loading = 'fail';
              });
            });
          } else {
          }
        }
      }
      setTimeout(function () {
        var inicBox = iElm.find('.inicBox');
        var sliderCount = function () {
          return iElm.find('.sls').children().length;
        };
        // console.log(inicBox);
        // if (iElm.find('.sls').children().length > 1) {
        function initSlides() {
          window.mySwipe = new Swipe(iElm[0], {
            speed: 300,
            auto: false,
            continuous: true,
            disableScroll: false,
            stopPropagation: false,
            callback: function (index, elem) {
              inicBox.removeClass('init');
              inicBox.find('i').removeClass('a');
              inicBox.find('.i' + index).addClass('a');
              if (mySwipe !== undefined) {
                if (mySwipe.getPos() == mySwipe.getNumSlides()) {
                  mySwipe.slide(0, 0);
                }
              }
              if (mySwipe !== undefined) {
                if (mySwipe.getPos() > mySwipe.getNumSlides()) {
                  mySwipe.slide(mySwipe.getNumSlides() - 1, 0);
                }
              }
            },
            transitionEnd: function (index, elem) {
              // if (loadedSlides['_'+index] === undefined) {
              loadContent(index, elem);  // }
            }
          });
        }
        ;
        initSlides();
        // } else {
        //     inicBox.css('display', 'none');
        // }
        if (iElm.find('.sls').children().length == 1) {
          inicBox.css('display', 'none');
        }
        setTimeout(function () {
          loadContent(0);
        }, 50);
        $('.ovl, .ovr').on('touchstart', function () {
          $(this).addClass('aa');
        }).on('touchend', function () {
          $(this).removeClass('aa');
        });  // setInterval(function(){
             //   $('.ovl, .ovr').hide(1).show(1);
             // }, 800);
      }, 1);
    }
  };
}).directive('googlemaps', function ($navigate) {
  // Runs during compile
  return {
    restrict: 'A',
    link: function ($scope, iElm, iAttrs, controller) {
      var setDetail = function () {
        localStorage.fromMenu = false;
        $scope.$apply($navigate.go('/detail.html'));
      };
      setTimeout(function () {
        if (fromDetailMapLink === 1) {
          $('.mapB').css('display', 'block');
        }
        fromDetailMapLink = 0;
        var markers = [];
        // var default_icon = 'img/fmi.png';
        var image = new google.maps.MarkerImage('img/fmi.png', new google.maps.Size(44, 45), new google.maps.Point(0, 0), new google.maps.Point(19, 43));
        var mapOptions = {
            center: new google.maps.LatLng(47.248288, 11.397876),
            zoom: 3,
            disableDefaultUI: true,
            zoomControl: true
          };
        var showuseronmap = function (pos) {
          var latitude = pos.coords.latitude;
          var longitude = pos.coords.longitude;
          userpos = [
            latitude,
            longitude
          ];
          var image = new google.maps.MarkerImage('img/me.png');
          var latlng = new google.maps.LatLng(userpos[0], userpos[1]);
          map.setCenter(latlng);
          map.setZoom(10);
          setTimeout(function () {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
              });
          }, 200);
        };
        var centerMapOn = function (coords) {
          var latlng = new google.maps.LatLng(coords[0], coords[1]);
          map.setCenter(latlng);
          map.setZoom(11);
        };
        var error_callback = function (e) {
        };
        // onError Callback receives a PositionError object
        //
        function onError(error) {
        }
        if (userpos == 'get') {
          navigator.geolocation.getCurrentPosition(showuseronmap, onError);  // userpos = '';
        }
        ;
        // geo_position_js.getCurrentPosition(showuseronmap, error_callback);
        if (camsNearByCenter !== undefined) {
          var mapLoad = setInterval(function () {
              if (map !== undefined) {
                clearTimeout(mapLoad);
                centerMapOn(camsNearByCenter);
              }
              ;
            }, 250);
        }
        ;
        var map = new google.maps.Map(iElm[0], mapOptions);
        var setMarker = function (data) {
          var latlng = new google.maps.LatLng(data[1], data[2]);
          var marker = new google.maps.Marker({
              position: latlng,
              map: map,
              title: data[0],
              icon: image,
              id: data[5]
            });
          var t = 0;
          google.maps.event.addListener(marker, 'click', function (e) {
            // if (t) {
            map.setCenter(marker.getPosition());
            var mk = this;
            for (var i = jsonOrte.length - 1; i >= 0; i--) {
              if (mk.id == jsonOrte[i].id) {
                // Go To Cam Detail
                camDetailIndex = jsonOrte[i];
                localStorage.camDetailIndex = JSON.stringify(jsonOrte[i]);
                setDetail();
                i = 0;
              }
            }  // };
          });
          markers.push(marker);
        };
        // $.getScript("http://www.feratel.com/typo3conf/ext/icc_feratelwebcam/Resources/Public/JS/mapData.js", function() {
        if (mapLocations !== undefined) {
          for (var i = mapLocations.length - 1; i >= 0; i--) {
            var t = mapLocations[i];
            setMarker(t);
          }
          var MCzoom = 8;
          var MCsize = 38;
        }
        ;
        var markerCluster = new MarkerClusterer(map, markers, {
            maxZoom: MCzoom,
            gridSize: MCsize
          });  // });
      }, 1);  // setTimeout(function(){
              //   var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
              //   var mapOptions = {
              //     zoom: 4,
              //     center: myLatlng
              //   }
              //   var map = new google.maps.Map($('.gmaps')[0], mapOptions);
              //   var marker = new google.maps.Marker({
              //       position: myLatlng,
              //       map: map,
              //       title: 'Hello World!'
              //   });
              // },1);
    }
  };
}).directive('fatbox', [function () {
    // Runs during compile
    return {
      scope: true,
      restrict: 'E',
      template: '<div class="fatBox {{pano}}" style="display: {{display}}"><div class="title">{{title}}</div><div class="image"><img src="{{image}}" /></div><div class="close" ng-tap="closeView()"></div><div class="scrollbar"><div class="bar"></div></div></div>',
      replace: true,
      link: function ($scope, iElm, iAttrs, controller) {
        $scope.toggleView = function (e) {
          $(iElm[0]).toggleClass('focused');
        };
        $scope.closeView = function () {
          $('.fatBox .image img').attr('style', '');
          $('.fatBox .image img').attr('src', '');
          $scope.display = 'none';
        };
        var ww = $(window).width(), imgW = 0;
        var tracksize = 20;
        var barWidth = 50;
        var posX = 0, posY = 0, scale = 1, last_scale;
        // rotation= 1, last_rotation;
        var rect = $(iElm[0]).find('img')[0];
        var scrollbar = $(iElm[0]).find('.bar')[0];
        var scrollbarC = $(iElm[0]).find('.scrollbar')[0];
        $scope.pinchzoom = function (url, pinch, limg) {
          var hammertime = 0;
          hammertime = Hammer(iElm[0], {
            transform_always_block: true,
            transform_min_scale: 1,
            drag_block_horizontal: true,
            drag_block_vertical: true,
            drag_min_distance: 0
          });
          var allowZoom = 1;
          var posX = 0, posY = 0, scale = 1, last_scale = 1, scaleCposX = 0, scaleCposY = 0, lastPosX = 0, lastPosY = 0, tf = 0, imgW = 0, imgH = 0, OriImgW = 0, OriImgH = 0, conW = 0, conH = 0;
          if (pinch == 'scroll') {
            allowZoom = 0;
            scrollbarC.style.display = 'block';
          } else {
            scrollbarC.style.display = 'none';
          }
          ;
          if (!Hammer.HAS_TOUCHEVENTS && !Hammer.HAS_POINTEREVENTS) {
            Hammer.plugins.fakeMultitouch();
          }
          $(rect).on('load', function () {
            OriImgW = imgW = $(rect).width();
            OriImgH = imgH = $(rect).height();
            imgW = $(this).width();
            $(this).css('display', 'block');
            barWidth = Math.round(ww * (ww / imgW));
            scrollbar.style.left = 0;
            scrollbar.style.marginLeft = 0;
            scrollbar.style.width = barWidth + 'px';
          });
          conW = $(window).width();
          conH = $(window).height();
          hammertime.on('drag touch transform release', function (ev) {
            switch (ev.type) {
            // case 'drag':
            //     posX = ev.gesture.deltaX + lastPosX;
            //     // posY = ev.gesture.deltaY;
            //     break;
            // case 'release':
            //     lastPosX = posX;
            //     break;
            case 'touch':
              last_scale = scale;
              // last_rotation = rotation;
              break;
            case 'drag':
              posX = lastPosX + ev.gesture.deltaX;
              posY = lastPosY + ev.gesture.deltaY;
              break;
            case 'release':
              lastPosX = lastPosX + ev.gesture.deltaX;
              lastPosY = lastPosY + ev.gesture.deltaY;
              break;
            case 'transform':
              if (allowZoom) {
                scale = Math.max(1, Math.min(last_scale * ev.gesture.scale, 10));
                imgW = Math.round(OriImgW * scale);
                imgH = Math.round(OriImgH * scale);
              }
              ;
              // posX =
              // scaleCposX = (OriImgW-)
              break;
            }
            if (scale >= 2.5) {
              scale = 2.5;
            }
            ;
            if (pinch == 'scroll') {
              allowZoom = 0;
            }
            ;
            var dfr = (imgW + posX - conW) * -1;
            var dfb = (imgH + posY - conH) * -1;
            if (dfr > 10) {
              posX = imgW * -1 + conW - 10;
            }
            ;
            if (dfb > 105) {
              posY = imgH * -1 + conH - 105;
            }
            ;
            if (posX > 10) {
              posX = 10;
            }
            ;
            if (posY > 40) {
              posY = 40;
            }
            ;
            var max = imgW - conW, is = posX * -1, ratio = is / max;
            var mloffset = Math.round(barWidth * ratio);
            if (mloffset < 0) {
              mloffset = 0;
            }
            // transform!
            var transform = 'translate3d(' + (scaleCposX + posX) + 'px,' + (scaleCposY + posY) + 'px, 0) ' + 'scale(' + scale + ') ';
            // if (limg !== undefined) {};
            rect.style.transform = transform;
            rect.style.oTransform = transform;
            rect.style.msTransform = transform;
            rect.style.mozTransform = transform;
            rect.style.webkitTransform = transform;
            scrollbar.style.left = Math.round(ratio * 100) + '%';
            scrollbar.style.marginLeft = '-' + mloffset + 'px';
            scrollbar.style.width = barWidth + 'px';  // if (posX > 0) {
                                                      //   posX = 0;
                                                      // }
                                                      // if (posX < (imgW - ww) * -1) {
                                                      //   posX = (imgW - ww) * -1;
                                                      // }
                                                      // var max = imgW - ww,
                                                      //     is = posX * -1,
                                                      //     ratio = is / max;
                                                      //     // barWidth = ;
                                                      // var mloffset = Math.round(barWidth * ratio);
                                                      // if (mloffset < 0) {mloffset = 0}
                                                      // // transform!
                                                      // var transform = 'translate3d(' + posX+'px,'+0+'px, 0) ';
                                                      // rect.style.transform = transform;
                                                      // rect.style.oTransform = transform;
                                                      // rect.style.msTransform = transform;
                                                      // rect.style.mozTransform = transform;
                                                      // rect.style.webkitTransform = transform;
                                                      // scrollbar.style.left = Math.round((ratio) * 100) + '%';
                                                      // scrollbar.style.marginLeft = '-' + mloffset + 'px';
                                                      // scrollbar.style.width = barWidth + 'px';
          });
        };  // fatbox;
      }
    };
  }]);
document.addEventListener('pause', onPause, false);
function onPause() {
  $('video').each(function () {
    $(this)[0].pause();
  });
}
document.addEventListener('backbutton', onBackKeyDown, false);
function onBackKeyDown(e) {
  e.preventDefault();
}
var onSuccess = function (position) {
};
// onError Callback receives a PositionError object
//
function onError(error) {
  customAlert(1, 'loc');
}
var devicereadyOK = 0;
function deviceready() {
  devicereadyOK = 1;
}
setTimeout(function () {
  if (!devicereadyOK) {
  }
}, 6000);
document.addEventListener('deviceready', deviceready, false);