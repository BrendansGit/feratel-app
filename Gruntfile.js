module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        // 'http-server': {
        //     dev: {
        //         // the server root directory
        //         root: 'app/',

        //         port: 9000,
        //         host: "127.0.0.1",

        //         showDir: true,
        //         autoIndex: true,
        //         defaultExt: "html",

        //         //wait or not for the process to finish
        //         runInBackground: true
        //     },
        //     preview: {
        //         // the server root directory
        //         root: 'build/',

        //         port: 9000,
        //         host: "127.0.0.1",

        //         showDir: true,
        //         autoIndex: true,
        //         defaultExt: "html",

        //         //wait or not for the process to finish
        //         runInBackground: true
        //     }
        // },

        connect: {
            dev: {
              options: {
                hostname: '*',
                //open: {
                  //   target: 'http://localhost:9000'
                //},
                port: 9000,
                base: 'app/',
              }
            },
            build: {
              options: {
                hostname: '*',
                port: 9001,
                base: 'phonegap/www/',
              }
            }
          },

        compass: {
            dist: {
                options: { // Target options
                    sassDir: 'app/styles',
                    cssDir: 'app/styles',
                    outputStyle: 'compressed'
                }
            }
        },
        ngmin: {
            controllers: {
                src: ['app/scripts/controllers/main.js'],
                dest: 'phonegap/www/js/controller.js'
            },
            directives: {
                expand: false,
                cwd: '',
                src: ['app/scripts/app.js'],
                dest: 'phonegap/www/js/app.js'
            }
        },
        uglify: {
            dist: {
                // files: {
                //     'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                // }
                files: [
                // {
                //     src: '.buildCache/js/angular.js',
                //     dest: 'phonegap/www/js/angular.js'
                // }, 
                {
                    src: '.buildCache/js/jquery.js',
                    dest: 'phonegap/www/js/jquery.js'
                }, 
                {
                    src: '.buildCache/js/other.js',
                    dest: 'phonegap/www/js/other.js'
                }
                ]
            }
        },
        // ngmin: {
        //   controllers: {
        //     src: ['app/scripts/controllers/main.js', 'app/scripts/app.js'],
        //     dest: 'test/generated/controllers/one.js'
        //   },
        //   directives: {
        //     expand: true,
        //     cwd: 'test/src',
        //     src: ['app/scripts/app.js'],
        //     dest: 'dist/scripts/app.js'
        //   }
        // },
        watch: {
            all: {
                files: ['app/**/*.scss'],
                tasks: ['compass'],
                options: {
                    livereload: true
                }
            },
            script: {
                files: 'app/**/*.js',
                options: {
                    livereload: true
                }
            },
            html: {
                files: 'app/**/*.html',
                options: {
                    livereload: true
                }
            }
        },
        shell: {
            build_ios: {
                options: { // Options
                    stdout: true
                },
                command: [
                    'cd phonegap',
                    'echo  ',
                    'echo \033[33mBuilding APP with cordova..\033[0m',
                    'cordova build ios',
                    'cd ..',
                    'echo  ',
                    'echo \033[33mBuild complete\033[0m'
                ].join('&&')
            },
            build_android: {
                options: { // Options
                    stdout: true
                },
                command: [
                    'cd phonegap',
                    'echo  ',
                    'echo \033[33mBuilding APP with cordova..\033[0m',
                    'cordova build android',
                    'cd ..',
                    'echo  ',
                    'echo \033[33mBuild complete\033[0m'
                ].join('&&')
            },
            run_android: {
                options: { // Options
                    stdout: true
                },
                command: [
                    'cd phonegap',
                    'echo  ',
                    'echo \033[33mBuilding APP with cordova..\033[0m',
                    'cordova run android',
                    'cd ..',
                    'echo  ',
                    'echo \033[33mBuild complete\033[0m'
                ].join('&&')
            }

        },
        concat: {
            basic_and_extras: {
                files: {
                    '.buildCache/js/jquery.js': ['app/bower_components/jQuery/jquery.js', 'app/scripts/plugins/swipe.js', 'app/scripts/plugins/jquery.scrollTo.min.js', 'app/scripts/plugins/hammer.min.js', 'app/scripts/plugins/hammer.fakemultitouch.js'],
                    '.buildCache/js/angular.js': ['app/bower_components/angular/angular.js', 'app/scripts/plugins/ui-router.js', 'app/scripts/plugins/mobile-nav.min.js', 'app/bower_components/angular-resource/angular-resource.js', 'app/bower_components/angular-sanitize/angular-sanitize.js'],
                    '.buildCache/js/other.js': ['app/scripts/plugins/maps.cluster.js', 'app/scripts/lang/lang.js'],
                    '.buildCache/js/app.main.js': ['app/scripts/appjs/app.js', 'app/scripts/controllers/main.min.js']
                },
            },
        },
        copy: {
            main: {
                files: [{
                        expand: true,
                        cwd: 'app/',
                        src: 'img/**',
                        dest: 'phonegap/www/'
                    }, 
                    {
                        expand: true,
                        cwd: 'app/',
                        src: 'res/**',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: 'app/',
                        src: '*.png',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: 'app/',
                        src: '*.jpg',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: 'app/',
                        src: 'icon.png',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: 'app/',
                        src: 'font/**',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: 'app/',
                        src: 'styles/main.css',
                        dest: 'phonegap/www/'
                    },
                    {
                        expand: true,
                        cwd: '.buildCache/js/',
                        src: 'angular.js',
                        dest: 'phonegap/www/js/'
                    }
                ]
            }
        },
        htmlmin: { // Task
            dist: { // Target
                options: { // Target options
                    removeComments: true,
                    collapseWhitespace: true,
                    removeOptionalTags: true
                },
                files: {
                    'phonegap/www/index.html': 'app/index_build.html',
                    'phonegap/www/views/detail.html': 'app/views/detail.html',
                    'phonegap/www/views/favoriten.html': 'app/views/favoriten.html',
                    'phonegap/www/views/main.html': 'app/views/main.html',
                    'phonegap/www/views/menu.html': 'app/views/menu.html',
                    'phonegap/www/views/options.html': 'app/views/options.html',
                    'phonegap/www/views/suche.html': 'app/views/suche.html',
                    'phonegap/www/views/suchemap.html': 'app/views/suchemap.html',
                    'phonegap/www/views/content/de/impressum.html': 'app/views/content/de/impressum.html',
                    'phonegap/www/views/content/de/sprache.html': 'app/views/content/de/sprache.html',
                    'phonegap/www/views/content/en/impressum.html': 'app/views/content/en/impressum.html',
                    'phonegap/www/views/content/en/sprache.html': 'app/views/content/en/sprache.html',
                    'phonegap/www/views/content/cz/impressum.html': 'app/views/content/cz/impressum.html',
                    'phonegap/www/views/content/cz/sprache.html': 'app/views/content/cz/sprache.html',
                    'phonegap/www/views/content/fr/impressum.html': 'app/views/content/fr/impressum.html',
                    'phonegap/www/views/content/fr/sprache.html': 'app/views/content/fr/sprache.html',
                    'phonegap/www/views/content/sk/impressum.html': 'app/views/content/sk/impressum.html',
                    'phonegap/www/views/content/sk/sprache.html': 'app/views/content/sk/sprache.html',
                    'phonegap/www/views/content/it/impressum.html': 'app/views/content/it/impressum.html',
                    'phonegap/www/views/content/it/sprache.html': 'app/views/content/it/sprache.html',
                }
            },
            ios: {
                //files: {
                    //'phonegap/config.xml': 'app/config_ios.xml'
                //}
            },
            android: {
                //files: {
                //    'phonegap/config.xml': 'app/config_android.xml'
                //}
            },
            dev: { // Another target
                files: {
                    'dist/index.html': 'src/index.html',
                    'dist/contact.html': 'src/contact.html'
                }
            }
        },
        clean: {
            build: {
                src: ["phonegap/www/"]
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-ngmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('dev', ['connect', 'watch']);

    grunt.registerTask('build', ['clean:build', 'copy:main', 'concat', 'uglify', 'ngmin', 'htmlmin:dist']);

    grunt.registerTask('run_android', ['clean:build', 'copy:main', 'concat', 'uglify', 'ngmin', 'htmlmin:dist', 'shell:run_android']);
    grunt.registerTask('android', ['clean:build', 'copy:main', 'concat', 'uglify', 'ngmin', 'htmlmin:dist', 'shell:build_android']);
    grunt.registerTask('ios', ['clean:build', 'copy:main', 'concat', 'uglify', 'ngmin', 'htmlmin:dist', 'shell:build_ios']);


};