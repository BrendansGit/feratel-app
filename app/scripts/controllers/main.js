'use strict';

app.controller('MainCtrl', function($scope, $navigate) {
    iframeLoaded = 1;
    pageScroll(1);


    localStorage.fromMenu = 0;
    menuCache.clickedMenu = undefined;
    fromDetailMapLink = undefined;
    $scope.t = t;
    $scope.homeContent = t.homeC;
    $scope.sucheContent = t.sucheC;

    // var ok = function(e){
    //   $scope.$apply($scope.sucheContent = e.timestamp);
    // }
    $scope.shareapp = function() {
        shareApp();
    }
    $scope.camsNearMe = function() {
        camsNearByCenter = undefined;
        userpos = 'get';
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
        // navigator.geolocation.getCurrentPosition(ok);
        $navigate.go('/suchemap.html');
    };
    $scope.setLang = function(nL) {

        // delete localStorage.jsonOrte;

        // nv = $navigate;

        setTimeout(function(){
            $navigate.go(nL+'/sprache.html');
        },400)

        localStorage.lang = nL;
        window.refreshLang();

        updateXMLdata(function(){
            setTimeout(function() {
                window.location.reload(true);
            }, 30);
        });

    };
})
    .controller('mapCtrl', function($scope) {
        iframeLoaded = 1;
        window.track('camsnearme');
        pageScroll(1);
        $scope.t = t;

    })
    .controller('customAlert', function($scope) {
        $scope.t = t;
        $scope.ok = function() {
            if (udevice == 'iPhone' || udevice == 'iPad') {
                openLinkO('https://itunes.apple.com/app/feratel.com-webcams/id820954938');
            }else{
                openLinkO('https://play.google.com/store/apps/details?id=com.ferate.icc');

            }
            localStorage['denied'] = rating.version;
            localStorage['rating_time'] = 20;
            customAlert(0);
        };
        $scope.no = function() {
            customAlert(0);
            localStorage['denied'] = rating.version;
            localStorage['rating_time'] = 20;
        };
        $scope.later = function() {
            customAlert(0);
        };
        $scope.retryconnection = function() {
            customAlert(0);
            delete localStorage.jsonOrte;
            jsonOrte = undefined;
            loadXMLdata();
        };
    })
    .controller('SuchePage', function($scope) {
        window.track('search');
        iframeLoaded = 1;
        pageScroll(1);
        localStorage.fromMenu = 0;
        $scope.t = t;
        jsonOrte = localStorage.jsonOrte;
        if (jsonOrte == undefined) {
            $scope.status = 'loading';
            $scope.level = 0;
            loadXMLdata($scope, 'suche');
            // if (loadXMLdata) {
            //   if (jsonOrte) {
            //     $scope.$apply(function() {
            //       $scope.status = 'fertig';
            //       $scope.ortMenuList = jsonOrte;
            //     });
            //   };
            // };

        } else {
            jsonOrte = jQuery.parseJSON(localStorage.jsonOrte);
            editData(jsonOrte);
            $scope.status = 'fertig';
            $scope.ortMenuList = jsonOrte;
        }
        // $scope.myFilter = function(startsWith) {
        //   var t = $scope.stext;
        //   t = t.toLowerCase();
        //   t = $.trim(t).replace(/ +/g, ' ').toLowerCase();
        //   return function(startsWith) {
        //         var name = (startsWith.name).replace(/\s+/g, ' ').toLowerCase();
        //         // var firstLetterResult = startsWith.name.substring(0,1).toLowerCase();
        //         // var firstLetterSearch = t.substring(0,1);
        //         // if (name.indexOf(t) !== -1 && firstLetterResult === firstLetterSearch){
        //         //     return startsWith.name;
        //         // }
        //         // return !~name.indexOf(t);
        //   }
        // }
        // $scope.filterExpr = { tester: true };


        $scope.customOrder = function(card) {
            var n = card.name;
            var s = $scope.stext;
            var first = [],
                second = [];
            var nn;

            // if (n.indexOf(s) !== -1) {
            //   first.push(n);
            // }else{
            //   second.push(n);
            // };
            nn = n.indexOf(s);
            card.oo = nn;
            return true;
        };

    }).controller('OrtMenu', function($scope, $navigate) {

        window.track('menue');

        iframeLoaded = 1;
        pageScroll(1);
        $scope.t = t;

        // jsonMenu = localStorage.jsonMenu;
        $scope.listFilter = function() {
        };
        $scope.setStyles = function(x) {

            var styles = '-webkit-transform: translate3d(-' + x + 'px, 0, 0);' +
                '-moz-transform: translate3d(-' + x + 'px, 0, 0);' +
                '-ms-transform: translate3d(-' + x + 'px, 0, 0);' +
                '-o-transform: translate3d(-' + x + 'px, 0, 0);' +
                'transform: translate3d(-' + x + 'px, 0, 0);';

            // $('.ortMenus ul').css('height', '300px');
            // $('.ortMenus ul.lvl' + level).css('height', 'auto');
            setTimeout(function() {
                $('.ortMenus').attr('style', styles);
            }, 1);
            scrollto(0, 1);
        };

        $scope.ortMenu = [];
        $scope.level = 0;

        if (jsonMenu == undefined) {
            $scope.status = 'loading';

            // $.getJSON('http://www.appicc.com/feratel/app/menu.php', function(data) {

            //   if (!data.exception) {
            //     localStorage.jsonMenu = JSON.stringify(data);
            //     jsonMenu=data;

            //     $scope.$apply(function() {
            //       $scope.status = 'fertig';
            //       $scope.ortMenu.lvl0 = jsonMenu;
            //     });
            //   }else{
            //     error(data);
            //   };
            // });
            loadXMLdata($scope, 'menu');

        } else {
            // $scope.$apply(function() {
            $scope.status = 'fertig';
            $scope.ortMenu.lvl0 = jsonMenu;
            // });
        }
        // if (loadXMLdata) {
        //     if (jsonMenu) {
        //       $scope.$apply(function() {
        //         $scope.status = 'fertig';
        //         $scope.ortMenu.lvl0 = jsonMenu;
        //       });
        //     };
        //   };

        var lastActMenu;


        $scope.gotoLastMenu = function(level) {
            var ww = $('.moList').width();
            var x = ww * (level - 1);
            actMenuLevel = (level - 1);
            // console.log(lastActMenu);
            actMenu = lastActMenu;
            // actMenu = jsonMenu[index];
            $scope.setStyles(x);
        };

        menuCache.sub = [];
        $scope.openSub = function(index, level) {

            // log(index+' '+level);
            var isCam = false;
            $scope.level = level;
            menuCache.level = level;

            if (level === 1) {
                actMenu = jsonMenu[index];
            }

            if (level > 1) {
                // if (actMenu.sub) {
                lastActMenu = actMenu;
                if (actMenu.sub !== undefined) {
                    actMenu = actMenu.sub[index];
                }
                // }
            }
            if (actMenu.sub !== undefined) {
                menuCache.sub[level - 1] = [index, level, actMenu.sub];
            };
            if (actMenu.sub === undefined) {
                isCam = true;
            }

            if (isCam) {
                var id = actMenu.item.rid;
                for (var i = jsonOrte.length - 1; i >= 0; i--) {
                    if (id === jsonOrte[i].id) {
                        camDetailIndex = jsonOrte[i];
                        localStorage.camDetailIndex = JSON.stringify(jsonOrte[i]);
                        i = 0;
                    }
                };

                $navigate.go('/detail.html');
            } else {
                // setTimeout(function(){
                    // $scope.$apply(function(){
                        // console.log(level);
                        // console.log($scope);
                        $scope.ortMenu['lvl' + level] = actMenu.sub;
                        var ww = $('.moList').width();
                        var x = ww * level;
                        actMenuLevel = level;
                        localStorage.fromMenu = true;
                        $scope.setStyles(x);
                    // },0)
                // })
            }
            localStorage.mC = JSON.stringify(menuCache);
        };

        $(window).on('resize', function() {
            var ww = $('.moList').width();
            var x = ww * (actMenuLevel);
            $scope.setStyles(x);
        });

        if (menuCache.clickedMenu) {
            var lastMenu;
            if (localStorage.mC !== undefined) {
                lastMenu = jQuery.parseJSON(localStorage.mC);
            }

            // console.log(lastMenu);
            if (lastMenu !== undefined) {
                for (var i = 0; i < lastMenu.sub.length; i++) {
                    if (lastMenu.sub[i][2] !== null) {
                        // console.log(lastMenu.sub[i][0], lastMenu.sub[i][1]);
                        // console.log(jsonMenu[0]);
                        $scope.openSub(lastMenu.sub[i][0], lastMenu.sub[i][1]);

                        var sub;

                        var ww = window.outerWidth;
                        if (ww == 0) {
                            ww = window.innerWidth;
                        }
                        var level = lastMenu.sub[i][1];

                        if (level == 1) {
                            sub = jsonMenu[lastMenu.sub[i][0]];
                        }
                        if (level == 2) {
                            sub = sub.sub[lastMenu.sub[i][0]];
                        }


                        $scope.ortMenu['lvl' + level] = sub.sub;
                        var x = ww * level;
                        actMenuLevel = level;
                        menuCache.level = level;
                        localStorage.fromMenu = true;
                        $scope.setStyles(x);
                        // $scope.openSub(lastMenu.sub[i][0], lastMenu.sub[i][1])
                    }
                }
            }
            // $scope.openSub(0, 1);
        }
        // GO TO SUB
        // $scope.ortMenuList = ;
    })
    .controller('Header', function($scope) {
        $scope.t = t;
    })
    .controller('Footer', function($scope) {
        $scope.t = t;
        $scope.bottomMenu = [{
            name: t.mHome,
            goTo: 'main.html',
            id: 'home'
        }, {
            name: t.mCams,
            goTo: 'suche.html',
            id: 'webcams'
        }, {
            name: t.mFavs,
            goTo: 'favoriten.html',
            id: 'my_cams'
        }, {
            name: t.mMehr,
            goTo: 'options.html',
            id: 'options'
        }];


        

        window.setFavCount = function(){
            var countfavs = 0;
            if (localStorage.camFavorites !== undefined) {
                countfavs = jQuery.parseJSON(localStorage.camFavorites).length;
            }
            var $me = $('.favCountF');
            if (countfavs == 0) {
                $me.fadeOut(200);
            }else{
                $me.fadeIn(200);
            }
            $me.text(countfavs);
        }

        setTimeout(function(){
            if ($('.favCountF').length) {
                $('.favCountF').remove();
            }

            $('.my_cams').append('<div class="favCountF"></div>');
            window.setFavCount();
        });
    })
    .controller('sucheCtrl', function($scope, DataSource) {
        iframeLoaded = 1;
        pageScroll(1);
        localStorage.fromMenu = 0;
        $scope.t = t;
        // var SOURCE_FILE = 'i.jsp.xml';
        // var xmlTransform = function(data) {
        //   // var x2js = new X2JS();
        //   // var json = x2js.xml_str2json( data );
        //   return json;
        // };

        // var setData = function( data ) {
        //   $scope.jsonOrte = data.feratel.co.pl;
        // };

        // DataSource.get(SOURCE_FILE, setData, xmlTransform);
    })
    .controller('DetailCtrl', function($scope, $navigate) {
        // TRACKING


        // TRACKING END
        loadedSlides.length = 0;
        loadedSlides = [];
        iframeLoaded = 0;
        pageScroll(1);
        setTimeout(function() {
            // touchScroll('Page');
            // var scroller = new TouchScroll(document.querySelector(".container.main"));
        }, 1)
        // get localstorage detail cam
        $scope.t = t;

        menuCache.fromMenu;
        if (localStorage.mC !== undefined) {
            menuCache.fromMenu = jQuery.parseJSON(localStorage.mC);
        };


        $scope.backToMenu = menuCache.fromMenu;
        $scope.fromMenu = localStorage.fromMenu;
        delete localStorage.fromMenu;

        menuCache.clickedMenu = false;
        $scope.goBackToMenu = function() {

            menuCache.clickedMenu = true;
            // localStorage.fromMenu = false;
            $navigate.go('/menu.html');
            // return false;

        };
        var fatBoxScope = angular.element($('.fatBox')).scope();

        var data;
        // console.log(localStorage.camDetailIndex);
        if (localStorage.camDetailIndex != undefined && localStorage.camDetailIndex != 'undefined') {
            data = JSON.parse(localStorage.camDetailIndex);
        }
        // // format subtitle
        if (data['usebooking'] == 'false') {
            setTimeout(function(){
                $('.slide').addClass('noBooking');
            })
        }
        // console.log(data.subtitle);
        // if (data == undefined) {
        //     window.location.href="";
        // }
        var subtitleA = data.subtitle.split(',');
        data.subtitle = '';
        subtitleA = subtitleA.reverse();
        for (var i = 0; i < subtitleA.length - 1; i++) {
            if (i !== 0) {
                data.subtitle += ' / ';
            }
            data.subtitle += subtitleA[i];
        }


        var detailData;

        if (jsonCams !== undefined) {
            for (var i = jsonCams.length - 1; i >= 0; i--) {


                var ide = jsonCams[i].data.id;
                if (ide == data.id) {
                    detailData = jsonCams[i];
                    i = 0;
                }
                if (i == 0) {
                }
            }
        };

        // console.log(detailData);

        if (detailData !== undefined) {
            for (var i = (detailData.cams.cam).length - 1; i >= 0; i--) {
            }
            for (var i = detailData.cams.cam.length - 1; i >= 0; i--) {
                detailData.cams.cam[i].data['info'] = (data.detail['110'][i][1]);
                detailData.cams.cam[i].data['exist'] = (data.detail['110'][i][2]);
                // console.log(data);
            }

            // sort cams
            var working = [];
            var broke = [];

            for (var i = 0; i < detailData.cams.cam.length; i++) {
                var ex = parseInt(detailData.cams.cam[i].data.exist);
                if (ex === 0) {
                    working.push(detailData.cams.cam[i]);
                }else if (ex < 0) {
                    broke.push(detailData.cams.cam[i]);
                }
            };

            $scope.cams = working.concat(broke);
        } else {
            // alert('not found');
        }


        window.saveWidth = function() {
            // var ww = $('.detailPage').width() - 30;
            var compensate = 30;
            var ww = $(window).width();
            var videoWidth = 350;
            var ratio = 0;

            setTimeout(function() {
                ww = $(window).width();
                var compensate = $(window).width() - ($('.normal_view').width());
                ww = ww - compensate;
                ratio = ww / videoWidth;
                $('.iframe').attr('style', 'transform: scale(' + ratio + '); -ms-transform: scale(' + ratio + '); -webkit-transform: scale(' + ratio + ');');
                // var ratioC = Math.round((ratio * 220) - 20);
                // $('.player').attr('style', 'height:' + ratioC + 'px');
            }, 1);
            setWidthSlide = 1;
        };
        saveWidth();

        $scope.activeQualitySet = function(nq, id, clicker) {
            $('.'+clicker).addClass('glow').siblings().removeClass('glow');
            $('#video_'+id).attr('src', nq);
        };

        $scope.playVideo = function(vidUrl) {
            // console.log("PLAY");
            window.track('play');
            // if (android) {
            //     cordova.plugins.videoPlayer.play(vidUrl);
            // }else{
                var options = {
                    successCallback: function() {

                    },
                    errorCallback: function(errMsg) {

                    }
                  };
                  window.plugins.streamingMedia.playVideo(vidUrl, options);
            // }
        };

        $scope.show_view = function(news, old) {
            // alert(news+' -> '+old);
            pageScroll(1);
            var setH = function() {
                pageScroll(0);
                var ot = $('.camWeather').offset().top;
                var wh = $(window).height();
                var fh = $('.footer').height();
                $('.camWeather').height(wh - ot - fh);
            };
            setTimeout(function() {
                if (android && news.indexOf('wv_') !== -1) {
                    setH();
                    $(window).on('resize', setH);
                };
            }, 150);
            if (news.indexOf('ng') == -1) {
                scrollto('.titleBox h4', 1);
            };
            if (news.indexOf('nv') !== -1) {
                $('.Page').removeClass('extrainfo');
                saveWidth();
            } else {
                $('.Page').addClass('extrainfo');
            }
            $('.' + news).attr('style', 'display:block');
            $('.' + old).attr('style', 'display:none');
            $('.buchen_form').css('display', 'none');

            if (old.indexOf('nv') > -1) {
                // canSwipe = 0;
                if (mySwipe !== undefined) {
                    mySwipe.suspend();
                }
            } else {
                // canSwipe = 1;
                if (mySwipe !== undefined) {
                    mySwipe.revive();
                }
            }
            return false;
        };

        $scope.id = data.id;
        $scope.title = data.name;
        $scope.locid = data.locid;
        $scope.subtitle = data.subtitle;

        // HISTORIE FATBOX
        $scope.fatBoxIt = function(title, url, pano, scrollbar) {
            fatBoxScope.display = 'block';
            fatBoxScope.title = title;
            // fatBoxScope.image = url;
            $('.fatBox .image img').attr('src', url);
            $('.fatBox .image img').attr('style', 'display:block');
            if (pano == 'pano') {
                fatBoxScope.pano = pano;
                fatBoxScope.pinchzoom(url, scrollbar, pano);
                if (scrollbar !== undefined) {
                    $('.scrollbar').css('display', 'block');
                };
            } else {
                fatBoxScope.pano = undefined;
                $('.scrollbar').css('display', 'none');
            }
        };
    })
    .controller('FavCtrl', function($scope) {
        redoFavorites();
        // window.track('favorites');
        iframeLoaded = 1;
        pageScroll(1);
        localStorage.fromMenu = 0;
        $scope.t = t;
        // jQuery.parseJSON(localStorage.camFavorites);
        // localStorage.camFavorites = JSON.stringify(favs);
        var favs = [];
        if (localStorage.camFavorites !== undefined) {
            favs = jQuery.parseJSON(localStorage.camFavorites);
        }


        // console.log(favs[0].name);
        // for (var i = favs.length - 1; i >= 0; i--) {
        //     var subtitleA = favs[i].subtitle.split(',');
        //     favs[i].sttle = '';
        //     subtitleA = subtitleA.reverse();
        //     for (var e = 0; e < subtitleA.length - 1; e++) {
        //         if (e !== 0) {
        //             favs[i].sttle += ' / ';
        //         }
        //         favs[i].sttle += subtitleA[e];
        //     }
        // }

        $scope.editIds = [];

        var removeFav = function(id) {
            var favs = jQuery.parseJSON(localStorage.camFavorites),
                index;

            for (var i = favs.length - 1; i >= 0; i--) {
                if (favs[i].id == id) {
                    index = i;
                }
            }

            favs.splice(index, 1);
            $scope.favs = favs;
            localStorage.camFavorites = JSON.stringify(favs);
        };

        $scope.editmode = 'no';

        var saveEdits = function() {
            for (var i = $scope.editIds.length - 1; i >= 0; i--) {
                removeFav($scope.editIds[i]);
            }
            $scope.editIds.length = 0;
        };

        $scope.edittext = t.edittext;
        $scope.editMode = function() {

            if ($scope.editIds.length > 0) {
                saveEdits();
            }

            if ($scope.editmode !== 'no') {
                $scope.editmode = 'no';
                $scope.edittext = t.edittext;
            } else {
                $scope.editmode = 'editmode';
                $scope.edittext = t.saveText;
            }

            window.setFavCount();
        };


        $scope.canceEdit = function() {
            $scope.editIds.length = 0;
            $scope.editmode = 'no';
            $scope.edittext = t.edittext;
            $('.hidden').fadeIn(200).removeClass('hidden');
        };

        $scope.delelteFav = function(id) {
            $scope.editIds.push(id);
            $('.fav_' + id).fadeOut(200).addClass('hidden');
            return false;
        };

        var moveInArray = function(array, from, to) {
            var tbm = array[from];
            array.splice(from, 1);
            array.splice(to, 0, tbm);


            return sortFavs(array);
        }

        $scope.moveDown = function(id) {
            if (id.$index < favs.length-1) {
                var favArray = favs;
                favArray = moveInArray(favArray, id.$index, id.$index+1);
                $scope.favs = favArray;
                favs = favArray;

                console.log(favs);
                localStorage.camFavorites = JSON.stringify(favArray);    
            };
            return false;
        };

        $scope.moveUp = function(id) {
            console.log(id.$index);
            if (id.$index > 0) {
                var favArray = favs;
                var favArray = moveInArray(favArray, id.$index, id.$index-1);
                $scope.favs = favArray;
                favs = favArray;

                console.log(favs);
                localStorage.camFavorites = JSON.stringify(favArray);   
            };
            return false;
        };



        function sortFavs(tmpfavs) {
            for (var i = 0; i < tmpfavs.length; i++) {
                if (i == 0) {
                    tmpfavs[i].okup = '';
                }else{
                    tmpfavs[i].okup = 'ok';
                }
                if (i == tmpfavs.length - 1) {
                    tmpfavs[i].okdown = '';
                }else{
                    tmpfavs[i].okdown = 'ok';
                }
            };


            localStorage.camFavorites = JSON.stringify(tmpfavs);
            delete localStorage.camNewFavs;


            return tmpfavs;
        }

        $scope.favs = sortFavs(favs);

        setTimeout(function() {

            if(window.location.hash.indexOf('main.html') !== -1 || window.location.hash == '#/') {
                window.track('home');
            }else{
                window.track('favorites');
            };



            var slider = $('.slsH');

            var slidesImages = slider.find('.image');
            // console.log(slidesImages);
            var lsImages = {};

            if (localStorage.homeI !== undefined) {
                // var lsImages = jQuery.parseJSON(localStorage.homeI);
            }
            

            function loadPVImage(index) {

                var id = favs[index].id;

                // console.log(lsImages[id]);
                var loadImage = 1;
                var maxage = 10 * 60 * 1000;
                var tnow = new Date().getTime();



                if (lsImages[id] !== undefined) {

                    // console.log(tnow - lsImages[id].timestamp, maxage);
                    // console.log(tnow - lsImages[id].timestamp < maxage);
                    if (tnow - lsImages[id].timestamp < maxage) {
                        // slidesImages[index] = 'loaded';
                        $scope.$apply(function(){
                            $scope.favs[index].pvimage = lsImages[id].image;
                        });

                        loadImage = 0;
                        // console.log("local");
                    }
                }


                if (loadImage) {


                    var element = $(slidesImages[index]);
                    if (slidesImages[index] !== 'loaded') {
                        slidesImages[index] = 'loaded';
                        var tImage = element.attr('loadPreviewImage');
                        // console.log(tImage);
                        // var getUrl = 'http://appicc.com/feratel/app/get.php?&lang=' + t.pageLang + '&l=' + encodeURIComponent(tImage);
                        var getUrl = 'http://appicc.com/feratel/app/get.php?v=2&lang=' + t.pageLang + '&l=' + encodeURIComponent(tImage);

                        // console.log(getUrl);
                        var reqto = 1;

                        // log(tImage);
                        

                        var req = $.getJSON(getUrl, function(e) {

                            if (!e.exception) {
                                $scope.$apply(function() {
                                    if (index < $scope.favs.length) {
                                        var pvimg;
                                        for (var i = 0; i < e.bilder.length; i++) {
                                            // console.log(e.bilder[i]);
                                            if (e.bilder[i]['t'] == '42') {
                                                pvimg = e.bilder[i]['is'];
                                            };
                                        };

                                        // console.log(pvimg);

                                        if (pvimg.indexOf('[{') !== -1) {
                                          var src = JSON.parse(pvimg);
                                          var es;
                                          for (var i = 0; i < src.length; i++) {

                                            if (src[i].t == '42') {
                                              es = src[i].is;
                                            }
                                          };
                                          pvimg = es;
                                        }



                                        $scope.favs[index].image = e.bilder;
                                        $scope.favs[index].pvimage = pvimg;
                                        lsImages[id] = {
                                                    image: pvimg,
                                                    timestamp: new Date().getTime()
                                                };

                                        // console.log(lsImages);

                                        localStorage.homeI = JSON.stringify(lsImages);

                                    };
                                });
                            } else {
                                // log(e);
                                clearInterval(intHomePics);
                            };
                            reqto = 0;
                        });

                }

                    
                    // setTimeout(function(){
                    //     if (reqto) {
                    //         req.abort();
                    //         console.log("timeout");
                    //         clearInterval(intHomePics);

                    //         $scope.$apply(function() {
                    //             if (index < $scope.favs.length) {
                    //                 $scope.favs[index].loadError = 'fail';
                    //             };
                    //         });
                    //     }
                    // }, 12000);
                };

                if (quickfix['pvimages'] !== undefined) {
                    quickfix['pvimages']();
                };
            }   

            if (favs.length >= 1) {
                loadPVImage(0);
            }
            if (favs.length > 1) {
                setTimeout(function(){
                    window.favSwipe = new Swipe(slider[0], {
                        speed: 300,
                        auto: false,
                        continuous: true,
                        disableScroll: false,
                        stopPropagation: false,
                        callback: function(index, elem) {
                            var inicBox = $('.inicBox');
                            inicBox.removeClass('init');
                            inicBox.find('i').removeClass('a');
                            inicBox.find('.i' + index).addClass('a');

                            if (favSwipe.getPos() == favSwipe.getNumSlides()) {
                                favSwipe.slide(0, 0);
                            }
                            if (favSwipe.getPos() > favSwipe.getNumSlides()) {
                                favSwipe.slide(favSwipe.getNumSlides() - 1, 0);
                            }
                        },
                        transitionEnd: function(index, elem) {
                            loadPVImage(index);
                            // loadContent(index, elem);
                        }
                    });
                }, 300);
            }else if (favs.length == 1) {
                $('.homeFavs').addClass('alone');
            }
            // .each(function(index) {
            //     loadPVImage($(this), index);
            // });

            setTimeout(function(){
                slider.css('display','block');
            },300)

        }, 100);
    })
    .controller('OptCtrl', function($scope, $navigate) {
        window.track('options');
        iframeLoaded = 1;
        pageScroll(1);
        localStorage.fromMenu = 0;
        $scope.t = t;
        $scope.clearLS = function() {
            localStorage.clear();
        };
        $scope.updateButton = function() {
            $navigate.go('/suche.html');
            setTimeout(function(){
                updateXMLdata();
            },200)
        };
    });