

var lsL = localStorage.lang;

var t = [];
var defaultLang = 'de';

var global_success = function(e) {
    var lang = e.value.toLowerCase();
    // alert('global success: '+lang);
    var setLang = 'de';
    if (lang.indexOf('en') !== -1) {
        setLang = 'en';
    }
    if (lang.indexOf('cz') !== -1) {
        setLang = 'cz';
    }
    if (lang.indexOf('fr') !== -1) {
        setLang = 'fr';
    }
    if (lang.indexOf('it') !== -1) {
        setLang = 'it';
    }
    if (lang.indexOf('sk') !== -1) {
        setLang = 'sk';
    }
    // alert(setLang);
    lsL = setLang;
    localStorage.lang = setLang;
    aler(refreshLang);
    refreshLang();
}
var global_fail = function(e) {
    alert('error: ' + e)
}


if (lsL == undefined) {
    // alert('undefined');
    if(navigator.globalization !== undefined){
        navigator.globalization.getPreferredLanguage(global_success, global_fail);
    }else{
        lsL = defaultLang;
        localStorage.lang = defaultLang;
    }
}

if (lsL == undefined || localStorage.lang == undefined) {
    lsL = defaultLang;
    localStorage.lang = defaultLang;
}


var refreshLang = function() {

lsL = localStorage.lang;

if (lsL === 'de') {
	
    localStorage.lang = 'de';
// DEUTSCH

    t.pageLang = 'de';
    t.pageLangT3ID = '0';

    t.recomend = 'Diese App weiterempfehlen';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar.';

    t.weatherc = 'Quelle: wetter24.de';

    // Content Home
    t.homeC = 'Willkommen auf<br />feratel.com Webcams.<br />Finde deine Lieblingskameras mit der Lupe oder per Karte. Deine gewählten Favoritenkameras werden dann hier angezeigt.';
    // Content Suche
    t.sucheC = 'Geben Sie Ihren gewünschten Ort ein. Sie können auch via Karte nach Webcams suchen.';

    t.connection_title = 'Verbindung fehlgeschlagen';
    t.connection_text = 'Es konnte keine Verbindung zu feratel.com hergestellt werden. <br />Bitte überprüfen Sie ihre Internetverbindung.';
    t.connection_retry = 'Wiederholen';

    t.location_title = 'Ortungsdienst Deaktiviert';
    t.location_text = 'Bitte aktivien Sie auf Ihrem Smartphone die Ortungsdienst für die Feratel Webcams App um diese Funktion nutzen zu können.';

    // reminder
    t.rate_yes = 'JA';
    t.rate_no = 'NEIN';
    t.rate_later = 'SPÄTER';
    t.rate_text = 'Wenn Ihnen unsere App gefällt, nehmen Sie sich bitte kurz Zeit, um uns eine Bewertung zu hinterlassen.';
    t.rate_title = 'feratel.com Webcams';

    /* MENU */

    // Orte / Webcams
    t.menuButton = 'Orte / Webcams';
    // Home
    t.mHome = 'Home';
    // Webcams
    t.mCams = 'Webcams';
    // My Cams
    t.mFavs = 'My Cams';
    // Mehr
    t.mMehr = 'Mehr';

    t.addToFav = ' zu Favoriten hinzugefügt';
    t.removeFromFav = ' von Favoriten entfernt';

    // zb. Innsbruck
    t.placeholder = 'z.B. Innsbruck';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'WETTER aktuell';
    // Wetter Prognose
    t.weatherProg = 'Prognose';
    // Zurück
    t.back = 'Zurück';
    // Details
    t.details = 'Details:';
    // heute
    t.today = 'Heute';
    // Historie
    t.historie = 'Historie';
    // Uhr
    t.uhr = 'Uhr';
    // Pisten
    t.pisten = 'Pisten';
    // Lifte
    t.lifte = 'Lifte';
    // Kamerainfo
    t.kamerinfo = 'Kamerainfo';
    // Ort
    t.ort = 'Ort';
    // height
    t.height = 'Standorthöhe';
    // standort
    t.standort = 'Standort';
    // kanton
    t.kanton = 'Kanton';
    t.bundesland = 'Bundesland';
    t.provinz = 'Provinz';
    t.staat = 'Staat';
    // land
    t.land = 'Land';
    // infrastruktur
    t.infra = 'Infrastruktur';
    // Weitere Infos
    t.moreinfo = 'Weitere Infos';
    // Kameras in der Nähe
    t.camNearCam = 'Kameras in der Nähe';
    // Share
    t.camShareT = 'Share';
    // Online Buchen
    t.onlineBuchen = 'Online Buchen';
    // Panorama Blick
    t.panoBlick = 'Panorama Blick';
    // pistenplan
    t.pistenplan = 'Pistenplan';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'Sie haben noch keine Favoriten ausgewählt';
    // Bearbeitem
    t.edittext = 'Bearbeiten';
    // Abbrechen
    t.cancelText = 'Abbrechen';
    // Speichern
    t.saveText = 'Fertig';


    /* OPTIONS */

    t.clf = 'Delete Local Files';
    t.bewerten = 'Bewerten';
    t.onFB = 'auf Facebook Empfehlen';
    t.onTW = 'auf twitter Empfehlen';
    t.shareText = 'Empfehlen / Teilen';
    t.infoL = 'Information';
    t.impre = 'Impressum';
    t.langT = 'Sprache';
    t.update = 'Update Kameras';


    /* SUCHE */

    // Webecamsuche
    t.livecamT = 'Webcamsuche';
    // ergebnisse für
    t.resT = 'Ergebisse für';
    // Kameras in der Nähe
    t.camNearBy = 'Kameras in der Nähe';

    // o-buchen
    var buchen = {}
    buchen.Anreise = 'Anreise';
    buchen.Nights = 'Nächte';
    buchen.Adults = 'Erwachsene';
    buchen.Children = 'Kinder';
    buchen.ChildAge = 'Kind alter';
    buchen.search = 'Suchen';

    t.buchen = buchen;


}else if (lsL === 'en') {


    // ENGLISH
    localStorage.lang = 'en';

    t.recomend = 'Share this App';

    t.addToFav = ' was added to Favorites';
    t.removeFromFav = ' was removed from Favorites';

    t.weatherc = 'Source: wetter24.de';

    t.pageLang = 'en';
    t.pageLangT3ID = '1';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar. EN';

    // Content Home
    t.homeC = 'Welcome to feratel.com webcams. Find your favorite cameras with the magnifier or by map. The favorite cameras you select are then displayed here.';
    // Content Suche
    t.sucheC = 'Enter your desired town. You can also search for webcams via maps';

    // reminder
    t.rate_yes = 'YES';
    t.rate_no = 'NO';
    t.rate_later = 'NOT NOW';
    t.rate_text = 'If you enjoy uing feratel.com Webcams, would you mind taking a moent to rate it? Thanks for your support!';
    t.rate_title = 'feratel.com Webcams';

    t.location_title = 'Location Service not available';
    t.location_text = 'Please enable the location service on your smartphone for the Feratel Webcams app to use this feature.';

    t.connection_title = 'Connection Failed';
    t.connection_text = 'Could not connect to feratel.com. <br /> Please check your Internet connection and try again.';
    t.connection_retry = 'Retry';

    /* MENU */

    // Orte / Livecams
    t.menuButton = 'Towns / Webcams';
    // Home
    t.mHome = 'Home';
    // Livecams
    t.mCams = 'Webcams';
    // My Cams
    t.mFavs = 'My Cams';
    // Mehr
    t.mMehr = 'More';

    // zb. Innsbruck
    t.placeholder = 'e.g. Innsbruck';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'current WEATHER';
    // Wetter Prognose
    t.weatherProg = 'Forecast';
    // Zurück
    t.back = 'Back';
    // Details
    t.details = 'Details:';
    // heute
    t.today = 'Today';
    // Historie
    t.historie = 'History';
    // Uhr
    t.uhr = '';
    // Pisten
    t.pisten = 'Ski pistes';
    // Lifte
    t.lifte = 'Lifts';
    // Kamerainfo
    t.kamerinfo = 'Camera Info';
    // Ort
    t.ort = 'Town';
    // height
    t.height = 'Location altitude';
    // standort
    t.standort = 'Location';
    // kanton
    t.kanton = 'Canton';
    t.bundesland = 'Federal state';
    t.provinz = 'Province';
    t.staat = 'State';
    // land
    t.land = 'Country';
    // infrastruktur
    t.infra = 'Infrastructure';
    // Weitere Infos
    t.moreinfo = 'More info';
    // Kameras in der Nähe
    t.camNearCam = 'Cameras nearby';
    // Share
    t.camShareT = 'Share';
    // Online Buchen
    t.onlineBuchen = 'Online booking';
    // Panorama Blick
    t.panoBlick = 'Panorama view';
    // pistenplan
    t.pistenplan = 'Ski trail map';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'You have not selected any favorites';
    // Bearbeitem
    t.edittext = 'Edit';
    // Abbrechen
    t.cancelText = 'Abort';
    // Speichern
    t.saveText = 'Save';


    /* OPTIONS */

    t.clf = 'Delete local files';
    t.bewerten = 'Rate on App Store';
    t.shareText = 'Recomend / Share';
    t.onFB = 'share on Facebook';
    t.onTW = 'share on Twitter';
    t.infoL = 'Information';
    t.impre = 'Legal notice';
    t.langT = 'Language';
    t.update = 'Update cameras';


    /* SUCHE */

    // Livecamsuche
    t.livecamT = 'Webcam Search';
    // ergebnisse für
    t.resT = 'results for ';
    // Kameras in der Nähe
    t.camNearBy = 'Cameras near me';
    
        // o-buchen
    var buchen = {}
    buchen.Anreise = 'Arrival';
    buchen.Nights = 'Nights';
    buchen.Adults = 'Adults';
    buchen.Children = 'Children';
    buchen.ChildAge = 'Child Age';
    buchen.search = 'Search';

    t.buchen = buchen;

}else if (lsL === 'sk') {
    // SLOWAK

    localStorage.lang = 'sk';
    t.pageLang = 'sk';
    t.pageLangT3ID = '6';

    t.recomend = 'Zdieľať túto aplikáciu';

    t.weatherc = 'wetter24.de';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar. SK';

    t.addToFav = ' bol pridaný k obľúbeným';
    t.removeFromFav = ' bol odstránený z obľúbených';

    // Content Home
    t.homeC = 'Vitajte na<br />feratel.com Webcams.<br />Nájdi si svoju obľúbenú kameru pomocou lupy alebo na mape.';
    // Content Suche
    t.sucheC = 'Zadajte si miesto podľa svojho želania. Webkamery môžete hľadať aj pomocou mapy.';

    // reminder
    t.rate_yes = 'YES';
    t.rate_no = 'NO';
    t.rate_later = 'NOT NOW';
    t.rate_text = 'If you enjoy uing feratel.com Webcams, would you mind taking a moent to rate it? Thanks for your support!';
    t.rate_title = 'feratel.com Webcams';

    t.location_title = 'Umiestnenie Služba nie je k dispozícii';
    t.location_text = 'Prosím, povoľte umiestnenie služby na vašom smartphone pre feratel Webkamery aplikáciu používať túto funkciu.';

    t.connection_title = 'Pripojenie zlyhalo';
    t.connection_text = 'Nedá sa pripojiť k feratel.com. <br /> Prosím skontrolujte svoje pripojenie k Internetu a skúste to znova.';
    t.connection_retry = 'skúsiť znova';

    /* MENU */

    // Orte / Webcams
    t.menuButton = 'Miesta / Webkamery';
    // Home
    t.mHome = 'home';
    // Webcams
    t.mCams = 'webkamery';
    // My Cams
    t.mFavs = 'moje kamery';
    // Mehr
    t.mMehr = 'viac';

    // zb. Innsbruck
    t.placeholder = 'napr Innsbruck';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'aktuálne POČASIE';
    // Wetter Prognose
    t.weatherProg = 'Predpoveď';
    // Zurück
    t.back = 'späť';
    // Details
    t.details = 'podrobnosti:';
    // heute
    t.today = 'dnes';
    // Historie
    t.historie = 'história';
    // Uhr
    t.uhr = '';
    // Pisten
    t.pisten = 'zjazdovky';
    // Lifte
    t.lifte = 'vleky';
    // Kamerainfo
    t.kamerinfo = 'Info pomocou kamery';
    // Ort
    t.ort = 'miesto';
    // height
    t.height = 'výška lokality';
    // standort
    t.standort = 'lokalita';
    // kanton
    t.kanton = 'kantón';
    t.bundesland = 'spolková krajina';
    t.provinz = 'provincia';
    t.staat = 'štát';
    // land
    t.land = 'krajina';
    // infrastruktur
    t.infra = 'infraštruktúra';
    // Weitere Infos
    t.moreinfo = 'ďalšie informácie';
    // Kameras in der Nähe
    t.camNearCam = 'kamery v blízkosti';
    // Share
    t.camShareT = 'share';
    // Online Buchen
    t.onlineBuchen = 'rezervovať online';
    // Panorama Blick
    t.panoBlick = 'panoramatický pohľad';
    // pistenplan
    t.pistenplan = 'mapka zjazdoviek';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'Ešte ste si nezvolili favoritov';
    // Bearbeitem
    t.edittext = 'spracovať';
    // Abbrechen
    t.cancelText = 'prerušiť';
    // Speichern
    t.saveText = 'hotovo';


    /* OPTIONS */

    t.clf = 'delete local files';
    t.bewerten = 'zhodnotiť';
    t.onFB = 'odporúčať na facebook';
    t.onTW = 'odporúčať na twitter';
    t.shareText = 'odporúčať / podeliť sa';
    t.infoL = 'informácia';
    t.impre = 'Impressum';
    t.langT = 'jazyk';
    t.update = 'aktualizované kamery';


    /* SUCHE */

    // Webecamsuche
    t.livecamT = 'hľadanie webkamier';
    // ergebnisse für
    t.resT = 'výsledky';
    // Kameras in der Nähe
    t.camNearBy = 'kamery v blízkosti';

        // o-buchen
    var buchen = {}
    buchen.Anreise = "Prílet";
     buchen.Nights = 'Nights';
     buchen.Adults = 'dospelí';
     buchen.Children = 'Deti';
     buchen.ChildAge = 'vek dieťaťa ';
     buchen.search = "Hľadať";

    t.buchen = buchen;


}else if (lsL === 'cz') {
    // Czech


    t.recomend = 'Sdílet tuto aplikaci';
    localStorage.lang = 'cz';
    t.pageLang = 'cz';
    t.pageLangT3ID = '9';

    t.weatherc = 'wetter24.de';

    t.addToFav = ' byl přidán k oblíbeným';
    t.removeFromFav = ' byl odstraněn z oblíbených';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar. CZ';

    // Content Home
    t.homeC = 'Vítejte na stránkách webových kamer feratel.com. Najdi své oblíbené kamery pomocí lupy nebo mapy. Tvé vybrané oblíbené kamery se zde pak zobrazí.';
    // Content Suche
    t.sucheC = 'Zadejte požadované místo. Webové kamery můžete také vyhledat pomocí mapy.';


    // reminder
    t.rate_yes = 'YES';
    t.rate_no = 'NO';
    t.rate_later = 'NOT NOW';
    t.rate_text = 'If you enjoy uing feratel.com Webcams, would you mind taking a moent to rate it? Thanks for your support!';
    t.rate_title = 'feratel.com Webcams';

    t.location_title = 'Umístění Služba není k dispozici';
    t.location_text = 'Prosím, povolte umístění služby na vašem smartphone prothe feratel Webkamery aplikaci používat tuto funkci.';

    t.connection_title = 'Připojení se nezdařilo';
    t.connection_text = 'Nelze se připojit k feratel.com. <br /> Prosím zkontrolujte své připojení k Internetu a zkuste to znovu.';
    t.connection_retry = 'Zkusit znovu';

    /* MENU */

    // Orte / Webcams
    t.menuButton = 'Webové kamery';
    // Home
    t.mHome = 'Domů';
    // Webcams
    t.mCams = 'Hledání';
    // My Cams
    t.mFavs = 'Moje kamery';
    // Mehr
    t.mMehr = 'Více';

    // zb. Innsbruck
    t.placeholder = 'např. Innsbruck';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'POČASÍ aktuálně';
    // Wetter Prognose
    t.weatherProg = 'Prognóza';
    // Zurück
    t.back = 'Zpět';
    // Details
    t.details = 'Podrobnosti:';
    // heute
    t.today = 'Dnes';
    // Historie
    t.historie = 'Historie';
    // Uhr
    t.uhr = '';
    // Pisten
    t.pisten = 'Sjezdovky';
    // Lifte
    t.lifte = 'Lanovky';
    // Kamerainfo
    t.kamerinfo = 'Informace o kameře';
    // Ort
    t.ort = 'Místo';
    // height
    t.height = 'Výška střediska';
    // standort
    t.standort = 'Středisko';
    // kanton
    t.kanton = 'Kanton';
    t.bundesland = 'Spolková země';
    t.provinz = 'Provincie';
    t.staat = 'Stát';
    // land
    t.land = 'Země';
    // infrastruktur
    t.infra = 'Infrastruktura';
    // Weitere Infos
    t.moreinfo = 'Další informace';
    // Kameras in der Nähe
    t.camNearCam = 'Kamery v blízkosti';
    // Share
    t.camShareT = 'Sdílet';
    // Online Buchen
    t.onlineBuchen = 'Objednat online';
    // Panorama Blick
    t.panoBlick = 'Panoramatický pohled';
    // pistenplan
    t.pistenplan = 'Mapa sjezdovek';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'Ještě jste nevybrali žádné oblíbené položky';
    // Bearbeitem
    t.edittext = 'Upravit';
    // Abbrechen
    t.cancelText = 'Přerušit';
    // Speichern
    t.saveText = 'Hotovo';


    /* OPTIONS */

    t.clf = 'Vymazat místní soubory';
    t.bewerten = 'Ohodnotit';
    t.onFB = 'Doporučit na Facebook';
    t.onTW = 'Doporučit na twitter';
    t.shareText = 'Doporučit / sdílet';
    t.infoL = 'Informace';
    t.impre = 'Impressum';
    t.langT = 'Jazyk';
    t.update = 'Aktualizovat kamery';


    /* SUCHE */

    // Webecamsuche
    t.livecamT = 'Vyhledání webové kamery';
    // ergebnisse für
    t.resT = 'Výsledky pro';
    // Kameras in der Nähe
    t.camNearBy = 'Kamery v blízkosti';


    var buchen = {};
buchen.Anreise = "Prílet"; 
buchen.Nights = 'Nights'; 
buchen.Adults = 'Dospělí'; 
buchen.Children = 'Deti'; 
buchen.ChildAge = 'vek dieťaťa'; 
buchen.search = "Hľadať"; 

t.buchen = buchen;

}else if (lsL === 'it') {
    // Italian

    t.recomend = 'Condividi questa app';
    localStorage.lang = 'it';
    t.pageLang = 'it';
    t.pageLangT3ID = '3';

    t.weatherc = 'wetter24.de';

    t.addToFav = ' è stato aggiunto ai preferiti';
    t.removeFromFav = ' è stato rimosso dai Preferiti';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar. IT';

    // Content Home
    t.homeC = 'Benvenuti sulle<br />webcam di feratel.com.<br />Trova le tue webcam preferite con la lente o sulla mappa. Le webcam preferite da te selezionate saranno visualizzate qui.';
    // Content Suche
    t.sucheC = 'Inserisci la tua località preferita. Puoi effettuare la ricerca delle webcam anche sulla mappa.';


    // reminder
    t.rate_yes = 'YES';
    t.rate_no = 'NO';
    t.rate_later = 'NOT NOW';
    t.rate_text = 'If you enjoy uing feratel.com Webcams, would you mind taking a moent to rate it? Thanks for your support!';
    t.rate_title = 'feratel.com Webcams';

    t.location_title = 'Posizione Servizio non disponibile';
    t.location_text = 'Si prega di attivare il servizio di localizzazione sullo smartphone per la Feratel Webcam app per utilizzare questa funzione.';

    t.connection_title = 'Connessione non riuscita';
    t.connection_text = 'Could not connect to feratel.com. <br /> Please check your Internet connection and try again.';
    t.connection_retry = 'Riprova';

    /* MENU */

    // Orte / Webcams
    t.menuButton = 'Località / Webcam';
    // Home
    t.mHome = 'Home';
    // Webcams
    t.mCams = 'Webcams';
    // My Cams
    t.mFavs = 'My Cams';
    // Mehr
    t.mMehr = 'Di più';

    // zb. Innsbruck
    t.placeholder = 'ad esempio Innsbruck ';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'METEO aggiornato';
    // Wetter Prognose
    t.weatherProg = 'Previsioni';
    // Zurück
    t.back = 'Indietro';
    // Details
    t.details = 'Dettagli:';
    // heute
    t.today = 'Oggi';
    // Historie
    t.historie = 'Cronologia immagini';
    // Uhr
    t.uhr = 'Ora';
    // Pisten
    t.pisten = 'Piste';
    // Lifte
    t.lifte = 'Skilift';
    // Kamerainfo
    t.kamerinfo = 'Info webcam';
    // Ort
    t.ort = 'Località';
    // height
    t.height = 'Altitudine ubicazione attuale';
    // standort
    t.standort = 'Ubicazione attuale';
    // kanton
    t.kanton = 'Cantone';
    t.bundesland = 'Regione';
    t.provinz = 'Provincia';
    t.staat = 'Stato';
    // land
    t.land = 'Paese';
    // infrastruktur
    t.infra = 'Infrastrutture';
    // Weitere Infos
    t.moreinfo = 'Ulteriori informazioni';
    // Kameras in der Nähe
    t.camNearCam = 'Webcam nei dintorni';
    // Share
    t.camShareT = 'Condividi';
    // Online Buchen
    t.onlineBuchen = 'Prenota online';
    // Panorama Blick
    t.panoBlick = 'Vista panoramica';
    // pistenplan
    t.pistenplan = 'Mappa delle piste';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'Non hai ancora aggiunto nessuna webcam tra i preferiti';
    // Bearbeitem
    t.edittext = 'Modifica';
    // Abbrechen
    t.cancelText = 'Interrompi';
    // Speichern
    t.saveText = 'Fine';


    /* OPTIONS */

    t.clf = 'Cancella file locali';
    t.bewerten = 'Dai un voto';
    t.onFB = 'Consiglia su Facebook';
    t.onTW = 'Consiglia su Twitter';
    t.shareText = 'Consiglia/condividi';
    t.infoL = 'Informazioni';
    t.impre = 'Note legali';
    t.langT = 'Lingua';
    t.update = 'Aggiorna webcam';


    /* SUCHE */

    // Webecamsuche
    t.livecamT = 'Cerca webcam';
    // ergebnisse für
    t.resT = 'Risultati per';
    // Kameras in der Nähe
    t.camNearBy = 'Webcam nei dintorni';


var buchen = {};
buchen.Anreise = "Prilet"; 
buchen.Nights = 'Notti'; 
buchen.Adults = 'dospelí'; 
buchen.Children = 'Deti'; 
buchen.ChildAge = 'Vek dieťaťa'; 
buchen.search = "Hľadať"; 

t.buchen = buchen;


}else if (lsL === 'fr') {
    // French

    t.recomend = 'Partager cette application';
    localStorage.lang = 'fr';
    t.pageLang = 'fr';
    t.pageLangT3ID = '2';

    t.weatherc = 'wetter24.de';

    t.addToFav = ' a été ajouté aux favoris';
    t.removeFromFav = ' a été retiré de favoris';

    t.imageLoadError = 'Vorschau derzeit<br> nicht verfügbar. FR';

    // Content Home
    t.homeC = 'Bienvenue sur<br />feratel.com Webcams.<br />Trouve ta caméra préférée à l’aide d’une loupe ou de la carte. Les caméras préférées que tu as sélectionnées s’affichent ici.';
    // Content Suche
    t.sucheC = 'Entrez le nom de la localité choisie. Vous pouvez également utiliser la carte pour rechercher des webcams.';


    // reminder
    t.rate_yes = 'YES';
    t.rate_no = 'NO';
    t.rate_later = 'NOT NOW';
    t.rate_text = 'If you enjoy uing feratel.com Webcams, would you mind taking a moent to rate it? Thanks for your support!';
    t.rate_title = 'feratel.com Webcams';

    t.location_title = 'Situation Service non disponible';
    t.location_text = 'Se il vous plaît activer le service de localisation sur votre smartphone pour la Feratel Webcams application d\'utiliser cette fonctionnalité.';

    t.connection_title = 'Échec de la connexion';
    t.connection_text = 'Impossible de se connecter à feratel.com. <br /> Se il vous plaît vérifier votre connexion Internet et essayez à nouveau.';
    t.connection_retry = 'Refaire';

    
    /* MENU */

    // Orte / Webcams
    t.menuButton = 'Localités / Webcams';
    // Home
    t.mHome = 'Accueil';
    // Webcams
    t.mCams = 'Webcams';
    // My Cams
    t.mFavs = 'Mes webcams';
    // Mehr
    t.mMehr = 'Plus';

    // zb. Innsbruck
    t.placeholder = 'par exemple Innsbruck';


    /* DETAIL */

    // Wetter titel
    t.weatherT = 'MÉTÉO actuelle';
    // Wetter Prognose
    t.weatherProg = 'Prévision';
    // Zurück
    t.back = 'Retour';
    // Details
    t.details = 'Détails:';
    // heute
    t.today = 'Aujourd‘hui';
    // Historie
    t.historie = 'Historique';
    // Uhr
    t.uhr = 'Heure';
    // Pisten
    t.pisten = 'Pistes';
    // Lifte
    t.lifte = 'Remontées mécaniques';
    // Kamerainfo
    t.kamerinfo = 'Info caméra';
    // Ort
    t.ort = 'Localité';
    // height
    t.height = 'Altitude de l‘emplacement';
    // standort
    t.standort = 'Emplacement';
    // kanton
    t.kanton = 'Canton';
    t.bundesland = 'État fédéré';
    t.provinz = 'Province';
    t.staat = 'État';
    // land
    t.land = 'Pays';
    // infrastruktur
    t.infra = 'Infrastructure';
    // Weitere Infos
    t.moreinfo = 'Autres infos';
    // Kameras in der Nähe
    t.camNearCam = 'Caméras à proximité';
    // Share
    t.camShareT = 'Partager';
    // Online Buchen
    t.onlineBuchen = 'Réserver en ligne';
    // Panorama Blick
    t.panoBlick = 'Vue panoramique';
    // pistenplan
    t.pistenplan = 'Plan des pistes';


    /* FAVORITEN */

    // Keine Favoriten
    t.noFavT = 'Vous n’avez pas encore sélectionné de favoris';
    // Bearbeitem
    t.edittext = 'Modifier';
    // Abbrechen
    t.cancelText = 'Annuler';
    // Speichern
    t.saveText = 'Terminé';


    /* OPTIONS */

    t.clf = 'Supprimer fichiers locaux';
    t.bewerten = 'Noter';
    t.onFB = 'Recommander sur Facebook';
    t.onTW = 'Recommander sur Twitter';
    t.shareText = 'Recommander / Partager';
    t.infoL = 'Information';
    t.impre = 'Mentions légales';
    t.langT = 'Langue';
    t.update = 'Actualisation caméras';


    /* SUCHE */

    // Webecamsuche
    t.livecamT = 'Recherche webcam';
    // ergebnisse für
    t.resT = 'Résultats pour';
    // Kameras in der Nähe
    t.camNearBy = 'Caméras dans les environs';

var buchen = {};
buchen.Anreise = "Prilet"; 
buchen.Nights = 'Nuits'; 
buchen.Adults = 'dospelí'; 
buchen.Children = 'Deti'; 
buchen.ChildAge = 'vek dieťaťa'; 
buchen.search = "Hľadať"; 

t.buchen = buchen;
}


// window.location.reload(true);
};

refreshLang();