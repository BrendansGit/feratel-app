var piwikbaseurl = 'http://webstat02.feratel.at/';
// var piwikbaseurl = 'http://appicc.com/piwik/piwik/';

 var _paq;

document.addEventListener("DOMContentLoaded", function(event) { 

  var device;
  if (iphone) {
      device = 'iPhone';
  }
  if (android) {
      device = 'Android';
  }

  var toTime = 30 * 1000;
  var trackinterval;
  var trackedID, trackedIDs = [];
  var trackedCams = [];
  var lastCamsTracked = 0;
  var actTrack = 0;
  var detalCdata = {};

  var totalTrackHistory = {};
  var lstTrack = new Date().getTime();

  function pauseall(){
    clearTimeout(trackinterval);
  }

  document.addEventListener("pause", pauseall, false);

  $(function(){

    t.ca_title = 'Blaa Custom Title DE';
    t.ca_text = 'Custom Content DE';

    if (t.pageLang == 'en') {
      t.ca_title = 'BLAAAA EN';
      t.ca_text = 'Custom Content EN';
    };


    // fix home favorites
    var homeFavs = function(){
      var $images = $('.homeSlider img');
      var int = setInterval(function(){
        var loaded = 0;
        $images.each(function(){
          var src= $(this).attr('src');

          if (typeof src !== 'undefined') {
            if (src.length > 0) {
              loaded++;
            };
          }
        })
        if (loaded == $images.length) {
          clearInterval(int);

          $images.each(function(){
            if ($(this).attr('src').indexOf('[{') !== -1) {
              var src = JSON.parse($(this).attr('src'));
              var es;
              for (var i = 0; i < src.length; i++) {

                if (src[i].t == '42') {
                  es = src[i].is;
                }
              };
              $(this).attr('src', es);
            }
              
          });
        }
      }, 300);
    };

    function trackCam(){

      trackedID = '-1', trackedIDs = [];
      // var camdetail = jQuery.parseJSON(localStorage.camDetailIndex);

      // var cdata = {
      //     'kameraid': camdetail.id,
      //     'sprache': t.pageLang,
      //     'portalname': 'APP_feratel.com',
      //     'area': 'cam',
      //     'other': device
      // };


      var adi = (jQuery.parseJSON(localStorage.camDetailIndex));


      if (lastCamsTracked !== adi.id) {
        trackedCams.length = 0;
        trackedCams = [];
      }

      lastCamsTracked = adi.id;


      clearInterval(trackinterval);
      trackinterval = setInterval(function(){
        $('.sls').children().each(function(){
          var os = $(this).offset().left;
          if (os == 0) {
            var id = $(this).attr('data-index');
            if (localStorage.camDetailIndex !== undefined) {
              var dinfo = adi.detail;
              var actid = parseInt(dinfo['110'][id][0]);
              var hasTracked = 1;

              for (var i = 0; i < trackedCams.length; i++) {
                if (trackedCams[i] == actid) {
                  hasTracked = 0;
                }
              }

              var dateNow = new Date().getTime();

              var latsTracked = Infinity;
              if (totalTrackHistory[actid] !== undefined) {
                latsTracked = dateNow - totalTrackHistory[actid];
                // console.log(totalTrackHistory[actid]);
              }

              if (((parseInt(trackedID) !== actid) && hasTracked) && latsTracked > toTime) {
                detalCdata.kameraid = dinfo['110'][id][0];
                trackedID = parseInt(dinfo['110'][id][0]);
                actTrack = id;
                trackedCams.push(trackedID);

                totalTrackHistory[actid] = new Date().getTime();




                var icctrack = 'http://appicc.com/feratel/app/icctrack.php?id='+trackedID;
                icctrack += '&n='+encodeURIComponent(adi.name);
                icctrack += '&ua='+encodeURIComponent(navigator.userAgent);
                icctrack += '&l='+localStorage.lang;

                if (typeof window.device !== 'undefined') {
                    var deviceID = window.device.uuid;
                    icctrack += '&uid='+deviceID
                }

                $.get(icctrack, function(e){
                });

                window.trackPiwik(detalCdata, 'cam');
              }
            }
          }
        });
      },500);

    }


    quickfix = {
      home: function(){
        homeFavs();
        // customAlert(1, 'custom');
      },
      cam: function(){
        trackCam();
        // customAlert(1, 'custom');
      }
    };
  });




  // FUNCTION

  // window.trackPiwik = function() {

  // };
  window.trackPiwik = function(cdata, cam) {

    if (cdata.kameraid.length == 0) {
      trackedCams.length = 0;
      trackedCams = [];
    }


    var isdetail = (window.location.hash).indexOf('detail.html');
    // if (typeof id !== 'undefined') {
    //   cdata.kameraid = detailInfo['110'][id][0];
    // };



    detalCdata = cdata;


    if (typeof cam != 'undefined') {


      if (localStorage.camDetailIndex !== undefined && isdetail > -1) {
        var detailInfo = (jQuery.parseJSON(localStorage.camDetailIndex)).detail;


        if (detailInfo['110'][actTrack] !== undefined) {
          cdata.kameraid = detailInfo['110'][actTrack][0];
        }

      }

      trackedID = cdata.kameraid;

      var pixel = '<img src="'+piwikbaseurl+'piwik.php?rec=1&amp;idsite='+cdata.kameraid;
      pixel += '&amp;cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22'+cdata.kameraid;
      pixel += '%22%5D%2C%5B%22Lang%22%2C%22'+cdata.sprache;
      pixel += '%22%5D%2C%5B%22Portal%22%2C%22'+cdata.portalname;
      pixel += '%22%5D%2C%5B%22Area%22%2C%22'+cdata.area;
      pixel += '%22%5D%2C%5B%22Other%22%2C%22'+cdata.other;
      pixel += '%22%5D%5D&amp;_cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22'+cdata.kameraid;
      pixel += '%22%5D%2C%5B%22Lang%22%2C%22'+cdata.sprache;
      pixel += '%22%5D%2C%5B%22Portal%22%2C%22'+cdata.portalname;
      pixel += '%22%5D%2C%5B%22Area%22%2C%22'+cdata.area;
      pixel += '%22%5D%2C%5B%22Other%22%2C%22'+cdata.other;
      pixel += '%22%5D%5D" style="border:0" alt="" />';

      try {
          var piwikTrackerCam = Piwik.getTracker(piwikbaseurl+'piwik.php', cdata.kameraid);
          piwikTrackerCam.setCustomVariable(1,"Cam",cdata.kameraid,"visit");
          piwikTrackerCam.setCustomVariable(2,"Lang",cdata.sprache,"visit");
          piwikTrackerCam.setCustomVariable(3,"Portal",cdata.portalname,"visit");
          piwikTrackerCam.setCustomVariable(4,"Area",cdata.area,"visit");
          piwikTrackerCam.setCustomVariable(5,"Other",cdata.other,"visit");
          piwikTrackerCam.setCustomVariable(1,"Cam",cdata.kameraid,"page");
          piwikTrackerCam.setCustomVariable(2,"Lang",cdata.sprache,"page");
          piwikTrackerCam.setCustomVariable(3,"Portal",cdata.portalname,"page");
          piwikTrackerCam.setCustomVariable(4,"Area",cdata.area,"page");
          piwikTrackerCam.setCustomVariable(5,"Other",cdata.other,"page");
          piwikTrackerCam.trackPageView();
          piwikTrackerCam.enableLinkTracking();
      }catch(err){
          // reportError(cdata);
      }



      if (cdata.kameraid.length > 0) {
        $('.trackPixel').html(pixel);
      }

    }
  };

  $(window).on('load', function(){

    _paq = _paq || [];
    (function(){ var u=piwikbaseurl;
    _paq.push(['setSiteId', 1]);
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.defer=true; g.async=true; g.src=u+'piwik.js';
    s.parentNode.insertBefore(g,s); })();

    var pixel = '<img src="'+piwikbaseurl+'piwik.php?rec=1&amp;idsite=534';
    pixel += '&amp;cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22';
    pixel += '%22%5D%2C%5B%22Lang%22%2C%22'+t.pageLang;
    pixel += '%22%5D%2C%5B%22Portal%22%2C%22'+portalname;
    pixel += '%22%5D%2C%5B%22Area%22%2C%22'+'home';
    pixel += '%22%5D%2C%5B%22Other%22%2C%22'+device;
    pixel += '%22%5D%5D&amp;_cvar=%5B%5B%22+%22%2C%22+%22%5D%2C%5B%22Cam%22%2C%22';
    pixel += '%22%5D%2C%5B%22Lang%22%2C%22'+t.pageLang;
    pixel += '%22%5D%2C%5B%22Portal%22%2C%22'+portalname;
    pixel += '%22%5D%2C%5B%22Area%22%2C%22'+'home';
    pixel += '%22%5D%2C%5B%22Other%22%2C%22'+device;
    pixel += '%22%5D%5D" style="border:0" alt="" />';
    try {
        var piwikTrackerCam = Piwik.getTracker(piwikbaseurl+"piwik.php", 534);
        piwikTrackerCam.setCustomVariable(1,"Cam",'',"visit");
        piwikTrackerCam.setCustomVariable(2,"Lang",t.pageLang,"visit");
        piwikTrackerCam.setCustomVariable(3,"Portal",portalname,"visit");
        piwikTrackerCam.setCustomVariable(4,"Area",'home',"visit");
        piwikTrackerCam.setCustomVariable(5,"Other",device,"visit");
        piwikTrackerCam.setCustomVariable(1,"Cam",'',"page");
        piwikTrackerCam.setCustomVariable(2,"Lang",t.pageLang,"page");
        piwikTrackerCam.setCustomVariable(3,"Portal",portalname,"page");
        piwikTrackerCam.setCustomVariable(4,"Area",'home',"page");
        piwikTrackerCam.setCustomVariable(5,"Other",device,"page");
        piwikTrackerCam.trackPageView();
        piwikTrackerCam.enableLinkTracking();
    }catch(err){
    }


    // // $('.trackPixel').html(pixel);
    $('.staticTrackPixel').html(pixel);

  })

});